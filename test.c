#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include "smpc.h"
#include "smpc_io.h"
#include "bitut.h"
#include "smpc_net.h"
int main(int argc, char const *argv[]) {
  if (argc < 3) {
    printf("%s\n", "use: ip_addr port");
    return 0;
  }

  printf("TEST NOW\n");
  smpc_graph* graph = NULL;
  int pid0 = 0;
  int pid1 = 0;
  int port = atoi(argv[2]);
  if ((pid0 = fork()) == 0) {
    int gens = smpc_net_create_gen_socket(port, SMPC_NET_AF_INET);
    int v = smpc_io_parse_graph_from_file(&graph,"circuit format specification.txt");
    printf("OUTCODE: %d\n", v);
    smpc_topological_sort(graph);
    graph->options = SMPC_GC_OPT_GRR_FX_HG;
    smpc_set_gc(graph);
    smpc_print_gc_stdout(graph, 0);
    uint8_t input = 0x06;
    int length = 4;
    if (smpc_net_snd_gc(gens, graph, &input, length, length) < 0) {
      close(gens);
      perror("smpc_net_snd_gc");
      exit(1);
    }
    exit(0);
  }
  if ((pid1 = fork()) == 0) {
    int evs;
    while((evs = smpc_net_create_ev_socket((char*)argv[1], port, SMPC_NET_AF_INET)) < 0){}
    printf("%d\n", evs);
    int v = smpc_io_parse_graph_from_file(&graph,"circuit format specification.txt");
    printf("OUTCODE: %d\n", v);
    smpc_topological_sort(graph);
    uint8_t input = 0x01;
    int length = 4;
    if (smpc_net_rcv_gc(evs, graph, &input, length, length) < 0) {
      close(evs);
      perror("smpc_net_rcv_gc");
      exit(1);
    }
    close(evs);
    char output;
    smpc_evaluate_offline_gc(graph,&output);
    smpc_print_gc_stdout(graph, 0);
    printf("Output of evaluation %02X\n",output);
    exit(0);
  }
  wait(NULL);
  wait(NULL);
  return 0;
}
