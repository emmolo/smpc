#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include "smpc.h"
#include "smpc_io.h"
#include "bitut.h"
#include "smpc_net.h"
#define  LENGTH 16
#define  ALENGTH 16
void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0) {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000L;
    } else {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return;
}

struct my_timespec{
  struct timespec parse;
  struct timespec garble;
  struct timespec total;
}my_timespec;

struct my_timespec server(int port) {
  smpc_graph* graph = NULL;
  struct my_timespec tot;
  struct timespec ta, tb, tresult;
  tot.total.tv_sec = 0L;
  tot.total.tv_nsec = 0L;
  tot.parse.tv_sec = 0L;
  tot.parse.tv_nsec = 0L;
  tot.garble.tv_sec = 0L;
  tot.garble.tv_nsec = 0L;
  clock_gettime(CLOCK_REALTIME, &ta);
  int v = smpc_io_parse_graph_from_cbmc_gc(&graph, "cbmc-out/", 0);
  clock_gettime(CLOCK_REALTIME, &tb);
  printf("OUTPUT: %d\n", v);
  timespec_diff(&ta, &tb, &tresult);
  tot.total.tv_sec += tresult.tv_sec;
  tot.total.tv_nsec += tresult.tv_nsec;
  if (tot.total.tv_nsec >= 1000000000L) {
      tot.total.tv_nsec -= 1000000000L;
      ++tot.total.tv_sec;
  }
  tot.parse.tv_sec = tresult.tv_sec;
  tot.parse.tv_nsec = tresult.tv_nsec;
  printf("Parse time:\t%ld seconds | %ld nanoseconds\n", tresult.tv_sec, tresult.tv_nsec);
  smpc_topological_sort(graph);
  int gens = smpc_net_create_gen_socket(port, SMPC_NET_AF_INET);
  graph->options = SMPC_GC_OPT_GRR_FX_HG;
  clock_gettime(CLOCK_REALTIME, &ta);
  smpc_set_gc(graph);
  clock_gettime(CLOCK_REALTIME, &tb);
  timespec_diff(&ta, &tb, &tresult);
  tot.total.tv_sec += tresult.tv_sec;
  tot.total.tv_nsec += tresult.tv_nsec;
  if (tot.total.tv_nsec >= 1000000000L) {
      tot.total.tv_nsec -= 1000000000L;
      ++tot.total.tv_sec;
  }
  tot.garble.tv_sec = tresult.tv_sec;
  tot.garble.tv_nsec = tresult.tv_nsec;
  printf("Garbling time:\t%ld seconds | %ld nanoseconds\n", tresult.tv_sec, tresult.tv_nsec);
  //smpc_print_gc_stdout(graph, 0);
  uint8_t input[4];
  memset(input, 0, sizeof(input));
  input[0] = 0x03;
  int length = LENGTH;
  if (smpc_net_snd_gc(gens, graph, input, ALENGTH, length) < 0) {
    close(gens);
    perror("smpc_net_snd_gc");
  }
  close(gens);
  return tot;
}

int main(int argc, char const *argv[]) {
  if (argc < 2) {
    printf("%s\n", "use: port");
    return 0;
  }
  int port = atoi(argv[1]);
  printf("TEST NOW\n");
  struct my_timespec tresult, tot;
  tot.total.tv_sec = 0L;
  tot.total.tv_nsec = 0L;
  tot.parse.tv_sec = 0L;
  tot.parse.tv_nsec = 0L;
  tot.garble.tv_sec = 0L;
  tot.garble.tv_nsec = 0L;
  for (size_t i = 0; i < 1; i++) {
    tresult = server(port);
    printf("\nTotal time %ld:\t%ld seconds | %ld nanoseconds\n", i, tresult.total.tv_sec, tresult.total.tv_nsec);
    tot.total.tv_sec += tresult.total.tv_sec;
    tot.total.tv_nsec += tresult.total.tv_nsec;
    if (tot.total.tv_nsec >= 1000000000L) {
        tot.total.tv_nsec -= 1000000000L;
        ++tot.total.tv_sec;
    }
    tot.parse.tv_sec += tresult.parse.tv_sec;
    tot.parse.tv_nsec += tresult.parse.tv_nsec;
    if (tot.parse.tv_nsec >= 1000000000L) {
        tot.parse.tv_nsec -= 1000000000L;
        ++tot.parse.tv_sec;
    }
    tot.garble.tv_sec += tresult.garble.tv_sec;
    tot.garble.tv_nsec += tresult.garble.tv_nsec;
    if (tot.garble.tv_nsec >= 1000000000L) {
        tot.garble.tv_nsec -= 1000000000L;
        ++tot.garble.tv_sec;
    }
  }
  /*
  tot.total.tv_sec = tot.total.tv_sec / 10L;
  tot.total.tv_nsec = tot.total.tv_nsec / 10L;
  tot.parse.tv_sec = tot.parse.tv_sec / 10L;
  tot.parse.tv_nsec = tot.parse.tv_nsec / 10L;
  tot.garble.tv_sec = tot.garble.tv_sec / 10L;
  tot.garble.tv_nsec = tot.garble.tv_nsec / 10L;
  printf("\n\nMiddle time parse:\t%ld seconds | %ld nanoseconds\n", tot.parse.tv_sec, tot.parse.tv_nsec);
  printf("\n\nMiddle time garble:\t%ld seconds | %ld nanoseconds\n", tot.garble.tv_sec, tot.garble.tv_nsec);
  printf("\n\nMiddle time total:\t%ld seconds | %ld nanoseconds\n", tot.total.tv_sec, tot.total.tv_nsec);*/
}
