#ifndef SMPC_H
#define SMPC_H
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>


#define SMPC_INITIALIZE_ERROR_RANDSEED_OS_FAILURE -1
#define SMPC_EVALUATION_ERROR_GC_NOT_SORTED -1
#define SMPC_SUCCESS 0
#define SMPC_KEY_LENGTH 256
#define SMPC_KEY_POINTER (SMPC_KEY_LENGTH-1)
#define SMPC_KEY_B_NUM (SMPC_KEY_LENGTH/8)
#define SMPC_LUT_B_NUM (SMPC_KEY_B_NUM+1)
#define SMPC_FAIL(x) (x < 0)

#define SMPC_EVGATE_TYPE_UNKNOWN -1
#define SMPC_EVGATE_TYPE_AND 0
#define SMPC_EVGATE_TYPE_OR  1
#define SMPC_EVGATE_TYPE_NAND  2
#define SMPC_EVGATE_TYPE_NOR  3
#define SMPC_EVGATE_TYPE_XOR  4
#define SMPC_EVGATE_TYPE_XNOR  5
#define SMPC_EVGATE_TYPE_NOT_A  6
#define SMPC_EVGATE_TYPE_NOT_B  7

#define SMPC_GC_OPT_NO 0
#define SMPC_GC_OPT_GRR 1
#define SMPC_GC_OPT_FX 2
#define SMPC_GC_OPT_GRR_FX 3
#define SMPC_GC_OPT_GRR_FX_HG 4

#define SMPC_NOT_NONE 0
#define SMPC_NOT_A 1
#define SMPC_NOT_B 2
#define SMPC_NOT_E 3

//Gate evaluable structure
struct wire;

typedef struct smpc_evgate{
  uint8_t  lut[4][SMPC_LUT_B_NUM];
  unsigned int id;
  int type;
  bool visited;
  int not;
  struct smpc_wire* in0;
  struct smpc_wire* in1;
  struct smpc_wire* out;
}smpc_evgate;

typedef struct smpc_evgatelist_node{
  smpc_evgate* key;
  struct smpc_evgatelist_node*  next;
  struct smpc_evgatelist_node*  prev;
}smpc_evgatelist_node;

typedef struct smpc_evgatelist{
  smpc_evgatelist_node* head;
  smpc_evgatelist_node* tail;
  size_t length;
}smpc_evgatelist;

typedef struct smpc_wire{
  smpc_evgate* back;
  smpc_evgatelist* forward;
  uint8_t v[SMPC_KEY_B_NUM];
  bool state;
  uint8_t k0[SMPC_KEY_B_NUM];
  uint8_t k1[SMPC_KEY_B_NUM];
}smpc_wire;

typedef struct smpc_wirelist_node{
  struct smpc_wirelist_node* prev;
  struct smpc_wirelist_node* next;
  smpc_wire* key;
}smpc_wirelist_node;

typedef struct smpc_wirelist{
  smpc_wirelist_node* head;
  smpc_wirelist_node* tail;
  size_t length;
}smpc_wirelist;

typedef struct smpc_graph{
  smpc_wirelist* input_wires;
  smpc_wirelist* output_wires;
  smpc_evgatelist* sorted_gates;
  uint8_t options;
}smpc_graph;

/*
  Input:       None
  Output:      Error Code, SMPC_SUCCESS on success.
  Description: Initializes scmp random engine.
*/
int smpc_initialize();
/*
  Input:       None
  Output:      smpc_evgatelist* contains an initialized list of evgate.
  Description: Set head and tail to NULL and length to 0.
*/
smpc_evgatelist* smpc_evgatelist_create();
/*
  Input:       smpc_evgatelist* list   contains a preallocated list of evgate.
               smpc_evgate* key        contains a preallocated evgate.
  Output:      None.
  Description: Insert key on tail of the list.
*/
void smpc_evgatelist_insert(smpc_evgatelist* list, smpc_evgate* key);
/*
  Input:       None
  Output:      smpc_wire* contains an initialized list of wire.
  Description: Set head and tail to NULL and length to 0.
*/
smpc_wirelist* smpc_wirelist_create();
/*
  Input:       smpc_wirelist* list   contains a preallocated list of wire.
               smpc_wire* key        contains a preallocated wire.
  Output:      None.
  Description: Insert key on tail of the list.
*/
void smpc_wirelist_insert(smpc_wirelist* list, smpc_wire* key);
/*
  Input:       smpc_graph* g   contains the graph to sort.
  Output:      None. It places the output in g->sorted_gates.
  Description: Build a topological sort of g gates.
*/
void smpc_topological_sort(smpc_graph* g);
/*
  Input:       smpc_graph* g     contains the graph to evaluate.
               uint8_t* input    contains the list of input values.
               uint8_t* output   contains the list of output values.
  Output:      None. It places the output in output.
  Description: Evaluate the logical circuit contained in g on input "input".
*/
void smpc_evaluate_offline(smpc_graph* g,uint8_t*input,uint8_t*output);
/*
  Input:       smpc_graph* g    contains the graph to garble.
               uint8_t grrFlag  if 0 not use grr3 technique.
  Output:      None. It places the output in g.
  Description: Garble the circuit contained in g.
*/
void smpc_set_gate_keys(smpc_graph* g, uint8_t grrFlag);
/*
  Input:       smpc_graph* g     contains the gatbled graph to evaluate.
               uint8_t* input    contains the list of input values.
               uint8_t* output   contains the list of output values.
  Output:      None. It places the output in output.
  Description: Evaluate the garbled circuit contained in g on input "input".
*/
int smpc_evaluate_offline_gc_no(smpc_graph* g, uint8_t*output);
/*
  Input:       smpc_graph* G   contains the graph to print.
               uint8_t fxFlag  if 0 print XOR gate's LUT.
  Output:      None.
  Description: Print gc tables and wire labels in the standard output.
*/
void smpc_print_gc_stdout(smpc_graph* g, uint8_t fxFlag);
/*
  Input:       smpc_graph* g    contains the graph to garble.
               uint8_t grrFlag  if 0 not use grr3 technique.
  Output:      None. It places the output in g.
  Description: Garble the circuit contained in g using freeXOR technique.
*/
void smpc_set_gate_keys_freeXOR(smpc_graph* g, uint8_t flag);
/*
  Input:       smpc_graph* g     contains the gatbled graph to evaluate.
               uint8_t* input    contains the list of input values.
               uint8_t* output   contains the list of output values.
  Output:      None. It places the output in output.
  Description: Evaluate the garbled circuit contained in g on input "input"
               that is builded using freeXOR technique.
*/
int smpc_evaluate_offline_gc_fx(smpc_graph* g, uint8_t* output, uint8_t HGflag);
int smpc_set_gc(smpc_graph* g);
int smpc_evaluate_offline_gc(smpc_graph* g, uint8_t* output);
uint8_t _smpc_test_bit(uint8_t* s, uint8_t i);
#endif
