CC=gcc
CFLAGS=-O1
LIBS=-lm -lnettle -lgmp -L./ -lsimpleot

OBJ = lib-misc.o smpc.o smpc_io.o smpc_net.o
DEPENDECIES = smpc.h bitut.h lib-misc.h smpc_io.h smpc_net.h

all: test server_test client_test
%.o: %.c
	$(CC) -g -c -o $@ $< $(CFLAGS)

test: test.o $(OBJ) $(DEPENDECIES)
	make -C ./SimpleOT
	cp ./SimpleOT/libsimpleot.a libsimpleot.a
	$(CC) -g -o $@ $^ $(CFLAGS) $(LIBS)
server_test: server_test.o $(OBJ) $(DEPENDECIES)
	$(CC) -g -o $@ $^ $(CFLAGS) $(LIBS)
client_test: client_test.o $(OBJ) $(DEPENDECIES)
	$(CC) -g -o $@ $^ $(CFLAGS) $(LIBS)
.PHONY: clean

clean:
	rm -f *.o
	make -C ./SimpleOT clean
