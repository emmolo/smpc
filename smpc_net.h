#ifndef SMPC_NET_H
#define SMPC_NET_H
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "smpc.h"

#define SMPC_NET_FAIL -1
#define SMPC_NET_SUCCESS 1
#define SMPC_NET_AF_LOCAL 0
#define SMPC_NET_AF_INET 1
#define SOCKET_NAME "/tmp/9Lq7BNBnBycd6nxy.socket"

int smpc_net_create_ev_socket(char* destAddr, int port, int type);
int smpc_net_create_gen_socket(int port, int type);
int smpc_net_snd_gc(int sock, smpc_graph* g, uint8_t* input, int alength, int length);
int smpc_net_rcv_gc(int sock, smpc_graph* g, uint8_t* input, int alength, int length);
#endif
