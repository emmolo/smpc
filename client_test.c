#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include "smpc.h"
#include "smpc_io.h"
#include "bitut.h"
#include "smpc_net.h"
#define  LENGTH 16
#define  ALENGTH 16
void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0) {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000L;
    } else {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return;
}

struct my_timespec{
  struct timespec parse;
  struct timespec rcv;
  struct timespec eval;
  struct timespec total;
}my_timespec;

struct my_timespec client(char* ip_addr, int port) {
  smpc_graph* graph = NULL;
  int evs;
  struct my_timespec tot;
  while((evs = smpc_net_create_ev_socket(ip_addr, port, SMPC_NET_AF_INET)) < 0){}
  printf("%d\n", evs);
  struct timespec ta, tb, tresult;
  tot.total.tv_sec = 0L;
  tot.total.tv_nsec = 0L;
  tot.parse.tv_sec = 0L;
  tot.parse.tv_nsec = 0L;
  tot.rcv.tv_sec = 0L;
  tot.rcv.tv_nsec = 0L;
  tot.eval.tv_sec = 0L;
  tot.eval.tv_nsec = 0L;
  clock_gettime(CLOCK_REALTIME, &ta);
  int v = smpc_io_parse_graph_from_cbmc_gc(&graph, "cbmc-out/", 0);
  clock_gettime(CLOCK_REALTIME, &tb);
  printf("OUTPUT: %d\n", v);
  timespec_diff(&ta, &tb, &tresult);
  tot.total.tv_sec += tresult.tv_sec;
  tot.total.tv_nsec += tresult.tv_nsec;
  if (tot.total.tv_nsec >= 1000000000L) {
      tot.total.tv_nsec -= 1000000000L;
      ++tot.total.tv_sec;
  }
  tot.parse.tv_sec = tresult.tv_sec;
  tot.parse.tv_nsec = tresult.tv_nsec;
  printf("Parse time:\t%ld seconds | %ld nanoseconds\n", tresult.tv_sec, tresult.tv_nsec);
  smpc_topological_sort(graph);
  graph->options = SMPC_GC_OPT_GRR_FX_HG;
  uint8_t input[4];
  memset(input, 0, sizeof(input));
  int length = LENGTH;
  input[1] = 0x02;
  clock_gettime(CLOCK_REALTIME, &ta);
  if (smpc_net_rcv_gc(evs, graph, input, ALENGTH, length) < 0) {
    close(evs);
    perror("smpc_net_rcv_gc");
  }
  clock_gettime(CLOCK_REALTIME, &tb);
  close(evs);
  timespec_diff(&ta, &tb, &tresult);
  tot.total.tv_sec += tresult.tv_sec;
  tot.total.tv_nsec += tresult.tv_nsec;
  if (tot.total.tv_nsec >= 1000000000L) {
      tot.total.tv_nsec -= 1000000000L;
      ++tot.total.tv_sec;
  }
  tot.rcv.tv_sec = tresult.tv_sec;
  tot.rcv.tv_nsec = tresult.tv_nsec;
  printf("Rcv time:\t%ld seconds | %ld nanoseconds\n", tresult.tv_sec, tresult.tv_nsec);
  char output;
  ClearAllU(&output);
  clock_gettime(CLOCK_REALTIME, &ta);
  smpc_evaluate_offline_gc(graph,&output);
  clock_gettime(CLOCK_REALTIME, &tb);
  timespec_diff(&ta, &tb, &tresult);
  tot.total.tv_sec += tresult.tv_sec;
  tot.total.tv_nsec += tresult.tv_nsec;
  if (tot.total.tv_nsec >= 1000000000L) {
      tot.total.tv_nsec -= 1000000000L;
      ++tot.total.tv_sec;
  }
  tot.eval.tv_sec = tresult.tv_sec;
  tot.eval.tv_nsec = tresult.tv_nsec;
  printf("Eval time:\t%ld seconds | %ld nanoseconds\n", tresult.tv_sec, tresult.tv_nsec);
  //smpc_print_gc_stdout(graph, 0);
  printf("Output of evaluation %02X\n",output);
  return tot;
}

int main(int argc, char const *argv[]) {
  if (argc < 3) {
    printf("%s\n", "use: ip_addr port");
    return 0;
  }
  int port = atoi(argv[2]);
  struct my_timespec tresult, tot;
  tot.total.tv_sec = 0L;
  tot.total.tv_nsec = 0L;
  tot.parse.tv_sec = 0L;
  tot.parse.tv_nsec = 0L;
  tot.rcv.tv_sec = 0L;
  tot.rcv.tv_nsec = 0L;
  tot.eval.tv_sec = 0L;
  tot.eval.tv_nsec = 0L;
  for (size_t i = 0; i < 1; i++) {
    tresult = client((char*)argv[1], port);
    printf("\nTotal time %ld:\t%ld seconds | %ld nanoseconds\n", i, tresult.total.tv_sec, tresult.total.tv_nsec);
    tot.total.tv_sec += tresult.total.tv_sec;
    tot.total.tv_nsec += tresult.total.tv_nsec;
    if (tot.total.tv_nsec >= 1000000000L) {
        tot.total.tv_nsec -= 1000000000L;
        ++tot.total.tv_sec;
    }
    tot.parse.tv_sec += tresult.parse.tv_sec;
    tot.parse.tv_nsec += tresult.parse.tv_nsec;
    if (tot.parse.tv_nsec >= 1000000000L) {
        tot.parse.tv_nsec -= 1000000000L;
        ++tot.parse.tv_sec;
    }
    tot.rcv.tv_sec += tresult.rcv.tv_sec;
    tot.rcv.tv_nsec += tresult.rcv.tv_nsec;
    if (tot.rcv.tv_nsec >= 1000000000L) {
        tot.rcv.tv_nsec -= 1000000000L;
        ++tot.rcv.tv_sec;
    }
    tot.eval.tv_sec += tresult.eval.tv_sec;
    tot.eval.tv_nsec += tresult.eval.tv_nsec;
    if (tot.eval.tv_nsec >= 1000000000L) {
        tot.eval.tv_nsec -= 1000000000L;
        ++tot.eval.tv_sec;
    }
  }
  /*
  tot.total.tv_sec = tot.total.tv_sec / 10L;
  tot.total.tv_nsec = tot.total.tv_nsec / 10L;
  tot.parse.tv_sec = tot.parse.tv_sec / 10L;
  tot.parse.tv_nsec = tot.parse.tv_nsec / 10L;
  tot.eval.tv_sec = tot.eval.tv_sec / 10L;
  tot.eval.tv_nsec = tot.eval.tv_nsec / 10L;
  tot.rcv.tv_sec = tot.rcv.tv_sec / 10L;
  tot.rcv.tv_nsec = tot.rcv.tv_nsec / 10L;
  printf("\n\nMiddle time parse:\t%ld seconds | %ld nanoseconds\n", tot.parse.tv_sec, tot.parse.tv_nsec);
  printf("\n\nMiddle time eval:\t%ld seconds | %ld nanoseconds\n", tot.eval.tv_sec, tot.eval.tv_nsec);
  printf("\n\nMiddle time rcv:\t%ld seconds | %ld nanoseconds\n", tot.rcv.tv_sec, tot.rcv.tv_nsec);
  printf("\n\nMiddle time total:\t%ld seconds | %ld nanoseconds\n", tot.total.tv_sec, tot.total.tv_nsec);*/
  return 0;
}
