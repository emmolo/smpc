#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/un.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <nettle/aes.h>
#include <time.h>
#include "smpc_net.h"
#include "SimpleOT/ot_receiver.h"
#include "SimpleOT/ot_sender.h"
#include "SimpleOT/ot_config.h"
#include "SimpleOT/randombytes.h"
#include "SimpleOT/network.h"
#include "SimpleOT/cpucycles.h"

int smpc_net_create_gen_socket(int port, int type){
  int sock;
  switch (type) {
    case SMPC_NET_AF_LOCAL:{
      struct sockaddr_un addr;
      unlink(SOCKET_NAME);
      sock = socket(AF_LOCAL, SOCK_SEQPACKET, 0);
      if (sock == -1) {
        perror("socket");
        return SMPC_NET_FAIL;
      }
      memset(&addr, 0, sizeof(struct sockaddr_un));
      addr.sun_family = AF_LOCAL;
      strncpy(addr.sun_path, SOCKET_NAME, sizeof(addr.sun_path) - 1);
      if (bind(sock, (const struct sockaddr *) &addr,
               sizeof(struct sockaddr_un)) < 0) {
        perror("bind");
        return SMPC_NET_FAIL;
      }
      break;
    }
    case SMPC_NET_AF_INET:{
      struct sockaddr_in addr;
      sock = socket(AF_INET, SOCK_STREAM, 0);
      if (sock == -1) {
        perror("socket");
        return SMPC_NET_FAIL;
      }
      memset(&addr, 0, sizeof(struct sockaddr_in));
      addr.sin_family = AF_INET;
      addr.sin_addr.s_addr = INADDR_ANY;
      addr.sin_port = htons(port);
      if (bind(sock, (struct sockaddr *) &addr,
              sizeof(addr)) < 0) {
        perror("bind");
        return SMPC_NET_FAIL;
      }
      break;
    }
    default:{
      return SMPC_NET_FAIL;
    }
  }
  if(listen(sock, 1) < 0) {
    perror("listen");
    return SMPC_NET_FAIL;
  }
  int csock = accept(sock, NULL, NULL);
  return csock;
}

int smpc_net_create_ev_socket(char* destAddr, int port, int type){
  int sock;
  switch (type) {
    case SMPC_NET_AF_LOCAL:{
      struct sockaddr_un addr;
      sock = socket(AF_LOCAL, SOCK_SEQPACKET, 0);
      if (sock == -1) {
        perror("socket");
        return SMPC_NET_FAIL;
      }
      memset(&addr, 0, sizeof(struct sockaddr_un));
      addr.sun_family = AF_LOCAL;
      strncpy(addr.sun_path, SOCKET_NAME, sizeof(addr.sun_path) - 1);
      if (connect(sock, (const struct sockaddr *) &addr,
                   sizeof(struct sockaddr_un)) < 0) {
        perror("connect");
        return SMPC_NET_FAIL;
      }
      break;
    }
    case SMPC_NET_AF_INET:{
      struct sockaddr_in addr;
      struct hostent* server;
      sock = socket(AF_INET, SOCK_STREAM, 0);
      if (sock < 0) {
        perror("socket");
        return SMPC_NET_FAIL;
      }
      server = gethostbyname(destAddr);
      if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        return SMPC_NET_FAIL;
      }
      memset(&addr, 0, sizeof(struct sockaddr_in));
      addr.sin_family = AF_INET;
      bcopy((char *)server->h_addr,
            (char *)&addr.sin_addr.s_addr,
            server->h_length);
      addr.sin_port = htons(port);
      if (connect(sock,(struct sockaddr *)&addr,sizeof(addr)) < 0) {
        perror("connect");
        return SMPC_NET_FAIL;
      }
      break;
    }
    default:{
      return SMPC_NET_FAIL;
    }
  }
  return sock;
}

int _smpc_net_snd_ot(SIMPLEOT_SENDER * sender, int sock,
                     smpc_wirelist* input_wires,
                     smpc_wirelist_node* m, int length) {
  unsigned char S_pack[PACKBYTES];
	unsigned char Rs_pack[4 * PACKBYTES];
  unsigned char keys[2][4][HASHBYTES];
  struct aes_ctx ctx;
  uint8_t ekeys[2][4][SMPC_KEY_B_NUM];
  smpc_wirelist_node* wnode = m;
  sender_genS(sender, S_pack);
  size_t j;
  //writing(sock, S_pack, sizeof(S_pack));
  if ((send(sock, S_pack, sizeof(S_pack), 0)) < 0) {
    perror("send");
    return SMPC_NET_FAIL;
  }
  for (j = 0; j < length; j += 4) {
    //recving(sock, Rs_pack, sizeof(Rs_pack));
    if ((recv(sock, Rs_pack, sizeof(Rs_pack), MSG_WAITALL)) < 0) {
      perror("recv");
      return SMPC_NET_FAIL;
    }
    sender_keygen(sender, Rs_pack, keys);
    for (size_t k = 0; k < 4; k++) {
      if ((j + k) < length) {
        aes_set_encrypt_key(&ctx, SMPC_KEY_B_NUM, (uint8_t*)keys[0][k]);
        aes_encrypt(&ctx, SMPC_KEY_B_NUM, ekeys[0][k] , wnode->key->k0);
        aes_set_encrypt_key(&ctx, SMPC_KEY_B_NUM, (uint8_t*)keys[1][k]);
        aes_encrypt(&ctx, SMPC_KEY_B_NUM, ekeys[1][k] , wnode->key->k1);
        if ((send(sock, ekeys[0][k], SMPC_KEY_B_NUM, 0)) < 0) {
          perror("send");
          return SMPC_NET_FAIL;
        }
        if ((send(sock, ekeys[1][k], SMPC_KEY_B_NUM, 0)) < 0) {
          perror("send");
          return SMPC_NET_FAIL;
        }
        wnode = wnode->next;
      }
    }
  }
  if ((2 * length) < input_wires->length) {
    for (j = (2 * length); j < input_wires->length; j++) {
      if (send(sock, (char*)wnode->key->k0, SMPC_KEY_B_NUM, 0) < 0) {
        perror("send");
        return SMPC_NET_FAIL;
      }
      wnode = wnode->next;
    }
  }
    return 0;
}

void timespec_diffa(struct timespec *start, struct timespec *stop,
                   struct timespec *result)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0) {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000L;
    } else {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return;
}

int smpc_net_snd_gc(int sock, smpc_graph* g, uint8_t* input, int alength, int length){
  unsigned int endSection = 0;
  smpc_evgatelist_node* p = g->sorted_gates->tail;

  if (send(sock, (char*)&g->options, sizeof(uint8_t), 0) < 0) {
    perror("send");
    return SMPC_NET_FAIL;
  }
  for (size_t i = 0; i < g->sorted_gates->length; i++) {
    switch (p->key->type) {
      case SMPC_EVGATE_TYPE_XOR:{
        if(g->options < (uint8_t)SMPC_GC_OPT_FX) {
          if(g->options == SMPC_GC_OPT_NO)
            if (send(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, 0) < 0) {
              perror("send");
              return SMPC_NET_FAIL;
            }
          if (send(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
          if (send(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
          if (send(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
        }
        break;
      }
      case SMPC_EVGATE_TYPE_XNOR:{
        if(g->options < (uint8_t)SMPC_GC_OPT_FX) {
          if(g->options == SMPC_GC_OPT_NO)
            if (send(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, 0) < 0) {
              perror("send");
              return SMPC_NET_FAIL;
            }
          if (send(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
          if (send(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
          if (send(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
        }
        break;
      }
      case SMPC_EVGATE_TYPE_AND:{
        if (g->options == SMPC_GC_OPT_NO) {
          if (send(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
        }
        if (send(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, 0) < 0) {
          perror("send");
          return SMPC_NET_FAIL;
        }
        if (g->options != SMPC_GC_OPT_GRR_FX_HG) {
          if (send(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
        }
        if (send(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, 0) < 0) {
          perror("send");
          return SMPC_NET_FAIL;
        }
        break;
      }
      case SMPC_EVGATE_TYPE_NAND:{
        if (g->options == SMPC_GC_OPT_NO) {
          if (send(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
        }
        if (send(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, 0) < 0) {
          perror("send");
          return SMPC_NET_FAIL;
        }
        if (g->options != SMPC_GC_OPT_GRR_FX_HG) {
          if (send(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
        }
        if (send(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, 0) < 0) {
          perror("send");
          return SMPC_NET_FAIL;
        }
        break;
      }
      default:{
        if(g->options == SMPC_GC_OPT_NO)
          if (send(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, 0) < 0) {
            perror("send");
            return SMPC_NET_FAIL;
          }
        if (send(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, 0) < 0) {
          perror("send");
          return SMPC_NET_FAIL;
        }
        if (send(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, 0) < 0) {
          perror("send");
          return SMPC_NET_FAIL;
        }
        if (send(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, 0) < 0) {
          perror("send");
          return SMPC_NET_FAIL;
        }
        break;
      }
    }
    p = p->prev;
  }

  smpc_wirelist_node* wi = g->input_wires->head;
  for (size_t i = 0; i < alength; i++) {
    if(_smpc_test_bit(input, i) == 0) {
      if (send(sock, (char*)wi->key->k0, SMPC_KEY_B_NUM, 0) < 0) {
        perror("send");
        return SMPC_NET_FAIL;
      }
    }
    else {
      if (send(sock, (char*)wi->key->k1, SMPC_KEY_B_NUM, 0) < 0) {
        perror("send");
        return SMPC_NET_FAIL;
      }
    }
    wi = wi->next;
  }
  SIMPLEOT_SENDER sender;

  struct timespec ta,tb, tresult;
  clock_gettime(CLOCK_REALTIME, &ta);
  _smpc_net_snd_ot(&sender, sock, g->input_wires, wi, length);
  clock_gettime(CLOCK_REALTIME, &tb);
  timespec_diffa(&ta, &tb, &tresult);
  printf("\nOT time:\t%ld seconds | %ld nanoseconds\n", tresult.tv_sec, tresult.tv_nsec);

  smpc_wirelist_node* wo = g->output_wires->head;
  char state;
  for (size_t i = 0; i < g->output_wires->length; i++) {
    if (wo->key->state) state = 'y';
    else state = 'n';
    if (send(sock, &state, sizeof(char), 0) < 0) {
      perror("send");
      return SMPC_NET_FAIL;
    }
    wo = wo->next;
  }

  return SMPC_NET_SUCCESS;
}

int _smpc_net_rcv_ot(SIMPLEOT_RECEIVER * receiver, int sock,
                     smpc_wirelist* input_wires, smpc_wirelist_node* m,
                     uint8_t* input, int length) {
  unsigned char Rs_pack[4 * PACKBYTES];
	unsigned char keys[4][HASHBYTES];
  unsigned char cb[4];
  struct aes_ctx ctx;
  uint8_t ekeys[2][4][SMPC_KEY_B_NUM];
  smpc_wirelist_node* wnode = m;
  //recving(sock, receiver->S_pack, sizeof(receiver->S_pack));
  if ((recv(sock, receiver->S_pack, sizeof(receiver->S_pack), MSG_WAITALL)) < 0) {
    perror("recv");
    return SMPC_NET_FAIL;
  }
  receiver_procS(receiver);
  receiver_maketable(receiver);
  size_t j;
  for (j = 0; j < length; j += 4) {
    cb[0] = _smpc_test_bit(input, j);
    if ((j + 1) < length) cb[1] = _smpc_test_bit(input, j+1);
    if ((j + 2) < length) cb[2] = _smpc_test_bit(input, j+2);
    if ((j + 3) < length) cb[3] = _smpc_test_bit(input, j+3);
    receiver_rsgen(receiver, Rs_pack, cb);
    //writing(sock, Rs_pack, sizeof(Rs_pack));
    if ((send(sock, Rs_pack, sizeof(Rs_pack), 0)) < 0) {
      perror("send");
      return SMPC_NET_FAIL;
    }
    receiver_keygen(receiver, keys);
    for (size_t k = 0; k < 4; k++) {
      if((j + k) < length) {
        if ((recv(sock, ekeys[0][k], SMPC_KEY_B_NUM, MSG_WAITALL)) < 0) {
          perror("recv");
          return SMPC_NET_FAIL;
        }
        if ((recv(sock, ekeys[1][k], SMPC_KEY_B_NUM, MSG_WAITALL)) < 0) {
          perror("recv");
          return SMPC_NET_FAIL;
        }
        aes_set_decrypt_key(&ctx, SMPC_KEY_B_NUM, (uint8_t*)keys[k]);
        if (_smpc_test_bit(input, j+k) == 0)
          aes_decrypt(&ctx, SMPC_KEY_B_NUM, wnode->key->v, ekeys[0][k]);
        else
          aes_decrypt(&ctx, SMPC_KEY_B_NUM, wnode->key->v, ekeys[1][k]);
        wnode = wnode->next;
      }
    }
  }
  for (j = (2 * length); j < input_wires->length; j++) {
    if (recv(sock, (char*)wnode->key->v, SMPC_KEY_B_NUM,MSG_WAITALL) < 0) {
      perror("recv");
      return SMPC_NET_FAIL;
    }
    wnode = wnode->next;
  }
  return 0;
}

void _smpc_net_evolve_not(smpc_evgatelist_node* p) {
  switch (p->key->not) {
    case SMPC_NOT_A: {
      switch (p->key->type) {
        case SMPC_EVGATE_TYPE_AND: {
          p->key->type = SMPC_EVGATE_TYPE_NOT_A;
          break;
        }
        case SMPC_EVGATE_TYPE_NAND: {
          p->key->type = SMPC_EVGATE_TYPE_NOT_A;
          break;
        }
        case SMPC_EVGATE_TYPE_XOR: {
          p->key->type = SMPC_EVGATE_TYPE_XNOR;
          break;
        }
        case SMPC_EVGATE_TYPE_XNOR: {
          p->key->type = SMPC_EVGATE_TYPE_XOR;
          break;
        }
      }
      break;
    }
    case SMPC_NOT_B: {
      switch (p->key->type) {
        case SMPC_EVGATE_TYPE_AND: {
          p->key->type = SMPC_EVGATE_TYPE_NOT_A;
          break;
        }
        case SMPC_EVGATE_TYPE_NAND: {
          p->key->type = SMPC_EVGATE_TYPE_NOT_A;
          break;
        }
        case SMPC_EVGATE_TYPE_XOR: {
          p->key->type = SMPC_EVGATE_TYPE_XNOR;
          break;
        }
        case SMPC_EVGATE_TYPE_XNOR: {
          p->key->type = SMPC_EVGATE_TYPE_XOR;
          break;
        }
      }
      break;
    }
    case SMPC_NOT_E: {
      switch (p->key->type) {
        case SMPC_EVGATE_TYPE_AND: {
          p->key->type = SMPC_EVGATE_TYPE_NOR;
          break;
        }
        case SMPC_EVGATE_TYPE_NAND: {
          p->key->type = SMPC_EVGATE_TYPE_OR;
          break;
        }
        case SMPC_EVGATE_TYPE_OR: {
          p->key->type = SMPC_EVGATE_TYPE_NAND;
          break;
        }
        case SMPC_EVGATE_TYPE_NOR: {
          p->key->type = SMPC_EVGATE_TYPE_AND;
          break;
        }
      }
      break;
    }
    default: {
      break;
    }
  }
}

int smpc_net_rcv_gc(int sock, smpc_graph* g, uint8_t* input, int alength, int length) {
  uint32_t ui = 0;
  uint8_t buffer[SMPC_LUT_B_NUM];
  smpc_evgatelist_node* p = g->sorted_gates->tail;
  if (recv(sock, (char*)&g->options, sizeof(uint8_t), MSG_WAITALL) < 0) {
    perror("recv");
    return SMPC_NET_FAIL;
  }
  for (size_t i = 0; i < g->sorted_gates->length; i++) {
    _smpc_net_evolve_not(p);
    switch (p->key->type) {
      case SMPC_EVGATE_TYPE_XOR: {
        if(g->options < (uint8_t)SMPC_GC_OPT_FX) {
          if(g->options == SMPC_GC_OPT_NO)
            if (recv(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
              perror("recv");
              return SMPC_NET_FAIL;
            }
          if (recv(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
          if (recv(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
          if (recv(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
        }
        break;
      }
      case SMPC_EVGATE_TYPE_XNOR: {
        if(g->options < (uint8_t)SMPC_GC_OPT_FX) {
          if(g->options == SMPC_GC_OPT_NO)
            if (recv(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
              perror("recv");
              return SMPC_NET_FAIL;
            }
          if (recv(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
          if (recv(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
          if (recv(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
        }
        break;
      }
      case SMPC_EVGATE_TYPE_AND:{
        if(g->options == SMPC_GC_OPT_NO)
          if (recv(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
        if (recv(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
          perror("recv");
          return SMPC_NET_FAIL;
        }
        if (g->options != SMPC_GC_OPT_GRR_FX_HG)
          if (recv(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
        if (recv(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
          perror("recv");
          return SMPC_NET_FAIL;
        }
        break;
      }
      case SMPC_EVGATE_TYPE_NAND:{
        if(g->options == SMPC_GC_OPT_NO)
          if (recv(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
        if (recv(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
          perror("recv");
          return SMPC_NET_FAIL;
        }
        if (g->options != SMPC_GC_OPT_GRR_FX_HG)
          if (recv(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
        if (recv(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
          perror("recv");
          return SMPC_NET_FAIL;
        }
        break;
      }
      default:{
        if(g->options == SMPC_GC_OPT_NO)
          if (recv(sock, (char*)p->key->lut[0], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
            perror("recv");
            return SMPC_NET_FAIL;
          }
        if (recv(sock, (char*)p->key->lut[1], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
          perror("recv");
          return SMPC_NET_FAIL;
        }
        if (recv(sock, (char*)p->key->lut[2], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
          perror("recv");
          return SMPC_NET_FAIL;
        }
        if (recv(sock, (char*)p->key->lut[3], SMPC_LUT_B_NUM, MSG_WAITALL) < 0) {
          perror("recv");
          return SMPC_NET_FAIL;
        }
        break;
      }
    }
    p = p->prev;
  }

  smpc_wirelist_node* wi = g->input_wires->head;
  for (size_t i = 0; i < alength; i++) {
    if (recv(sock, (char*)wi->key->v, SMPC_KEY_B_NUM, MSG_WAITALL) < 0) {
      perror("recv");
      return SMPC_NET_FAIL;
    }
    wi = wi->next;
  }
  SIMPLEOT_RECEIVER receiver;

  struct timespec ta,tb,tresult;
  clock_gettime(CLOCK_REALTIME, &ta);
  _smpc_net_rcv_ot(&receiver, sock, g->input_wires, wi, input, length);
  clock_gettime(CLOCK_REALTIME, &tb);
  timespec_diffa(&ta, &tb, &tresult);
  printf("\nOT time:\t%ld seconds | %ld nanoseconds\n", tresult.tv_sec, tresult.tv_nsec);

  smpc_wirelist_node* wo = g->output_wires->head;
  char state;
  for (size_t i = 0; i < g->output_wires->length; i++) {
    if (recv(sock, &state, sizeof(char), MSG_WAITALL) < 0) {
      perror("recv");
      return SMPC_NET_FAIL;
    }
    if (state == 'y') wo->key->state = true;
    else wo->key->state = false;
    wo = wo->next;
  }

  return 0;
}
