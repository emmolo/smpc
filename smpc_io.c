#include <errno.h>
#include <string.h>
#include "smpc_io.h"
#include <nettle/sha3.h>

#define SMPC_IO_PARSE_LINE_BUFFER_SIZE 100000L

/*
  Reads a line into linebuf from file f skipping all the empty lines before.
  linebuf must be of dimension SMPC_IO_PARSE_LINE_BUFFER_SIZE.
*/
int _smpc_io_readline(char* linebuf, FILE* f) {
  if (feof(f)) return -1;
  while (fgets(linebuf,SMPC_IO_PARSE_LINE_BUFFER_SIZE,f) != NULL && linebuf[0] == '\n') ;
  return 0;
}

bool _smpc_io_string_starts_with(char* str, char* prefix) {
  if (strncmp(str,prefix,strlen(prefix))==0) return true;
  return false;
}

int _smpc_io_parse_graph_gates_section(smpc_graph** graph, smpc_evgate** gates,
                                       int gatesCount,FILE* f, char* line) {
  int gateIndex;
  for (int i = 0; i < gatesCount; ++i) {
    _smpc_io_readline(line,f);
    if (feof(f)) return SMPC_IO_ERROR_PARSE_GATES_EXPECTED_GATE;
    char* endOfIndex;
    gateIndex = strtoul(line, &endOfIndex, 10);
    if (errno == EINVAL) return SMPC_IO_ERROR_PARSE_GATES_INVALID_INDEX;
    if (gateIndex != i) return SMPC_IO_ERROR_PARSE_GATES_INDEX_ORDER;
    while (*endOfIndex == ' ') ++endOfIndex;
    int type = 0;
    if (_smpc_io_string_starts_with(endOfIndex,"AND")) type = SMPC_EVGATE_TYPE_AND;
    else if (_smpc_io_string_starts_with(endOfIndex,"OR")) type = SMPC_EVGATE_TYPE_OR;
    else if (_smpc_io_string_starts_with(endOfIndex,"NAND")) type = SMPC_EVGATE_TYPE_NAND;
    else if (_smpc_io_string_starts_with(endOfIndex,"NOR")) type = SMPC_EVGATE_TYPE_NOR;
    else if (_smpc_io_string_starts_with(endOfIndex,"XOR")) type = SMPC_EVGATE_TYPE_XOR;
    else if (_smpc_io_string_starts_with(endOfIndex,"XNOR")) type = SMPC_EVGATE_TYPE_XNOR;
    else return SMPC_IO_ERROR_PARSE_GATES_INVALID_TYPE;
    gates[i] = (smpc_evgate*)malloc(sizeof(smpc_evgate));
    memset(gates[i]->lut,0,sizeof(gates[i]->lut));
    gates[i]->type = type;
    gates[i]->in0 = NULL;
    gates[i]->in1 = NULL;
    gates[i]->out = NULL;
    gates[i]->visited = false;
    gates[i]->id = 0;
    gates[i]->not = SMPC_NOT_NONE;
  }
  return SMPC_SUCCESS;
}

int _smpc_io_parse_graph_input_section(smpc_graph** graph,smpc_evgate** gates,
                                       int gatesCount,FILE* f, char* line) {
  _smpc_io_readline(line,f);
  int i = 0;
  while(!_smpc_io_string_starts_with(line,"Wires")) {
    if (feof(f)) return SMPC_IO_ERROR_PARSE_INPUT_EXPECTED_WIRES;
    char* endOfIndex;
    int inputIndex = strtoul(line, &endOfIndex, 10);
    if (errno == EINVAL) return SMPC_IO_ERROR_PARSE_INPUT_INVALID_INDEX;
    if (inputIndex != i) return SMPC_IO_ERROR_PARSE_INPUT_INDEX_ORDER;
    while (*endOfIndex == ' ') ++endOfIndex;
    if (*endOfIndex != '{') return SMPC_IO_ERROR_PARSE_INPUT_OBRACKET_EXPECTED;
    ++endOfIndex;
    smpc_wire* wire = (smpc_wire*)malloc(sizeof(smpc_wire));
    wire->back = NULL;
    wire->forward = smpc_evgatelist_create();
    memset(wire->v, 0, sizeof(wire->v));
    wire->state = false;
    while (*endOfIndex != 0){
      while(*endOfIndex == ' ') ++endOfIndex;
      int startingGate = strtoul(endOfIndex, &endOfIndex, 10);
      if (errno == EINVAL) return SMPC_IO_ERROR_PARSE_INPUT_INVALID_GATE;
      if (startingGate < 0 || startingGate >= gatesCount)
        return SMPC_IO_ERROR_PARSE_INPUT_GATE_OUT_OF_RANGE;
      smpc_evgatelist_insert(wire->forward, gates[startingGate]);
      if(gates[startingGate]->in0 == NULL) gates[startingGate]->in0 = wire;
      else if (gates[startingGate]->in1 == NULL) gates[startingGate]->in1 = wire;
      else return SMPC_IO_ERROR_PARSE_INPUT_TOO_MANY_GATE_INPUT_LINES;
      while(*endOfIndex == ' ') ++endOfIndex;
      if (*endOfIndex == '}') break;
      else if (*endOfIndex != ',') return SMPC_IO_ERROR_PARSE_INPUT_INVALID_CHAR;
      ++endOfIndex;
    }
    if (*endOfIndex == 0) return SMPC_IO_ERROR_PARSE_INPUT_CBRACKET_EXPECTED;
    smpc_wirelist_insert((*graph)->input_wires,wire);
    _smpc_io_readline(line,f);
    ++i;
  }
  return SMPC_SUCCESS;
}

int _smpc_io_parse_graph_wires_section(smpc_graph** graph,smpc_evgate** gates,
                                       int gatesCount,FILE* f, char* line) {
  _smpc_io_readline(line,f);
  int i = 0;
  while(!_smpc_io_string_starts_with(line,"Output")) {
    if (feof(f)) return SMPC_IO_ERROR_PARSE_WIRES_EXPECTED_OUTPUT;

    if (*line != '(') return SMPC_IO_ERROR_PARSE_WIRES_LPARENS_EXPECTED;
    ++line;
    while(*line == ' ') ++line;
    char* endOfNumber, *endOfNumber2;
    int srcID = strtoul(line, &endOfNumber, 10);
    if (errno == EINVAL) return SMPC_IO_ERROR_PARSE_WIRES_PARAM1_INVALID;
    while(*endOfNumber == ' ') ++endOfNumber;
    if (*endOfNumber != ',') return SMPC_IO_ERROR_PARSE_WIRES_COMMA_EXPECTED;
    endOfNumber++;
    int dstID = strtoul(endOfNumber, &endOfNumber2, 10);
    if (errno == EINVAL) return SMPC_IO_ERROR_PARSE_WIRES_PARAM2_INVALID;
    while(*endOfNumber2 == ' ') endOfNumber2++;
    if (*endOfNumber2 != ')') return SMPC_IO_ERROR_PARSE_WIRES_RPARENS_EXPECTED;
    if (srcID < 0 || srcID >= gatesCount) return SMPC_IO_ERROR_PARSE_WIRES_PARAM1_OUT_OF_RANGE;
    if (dstID < 0 || dstID >= gatesCount) return SMPC_IO_ERROR_PARSE_WIRES_PARAM2_OUT_OF_RANGE;
    if (!gates[srcID]->out) {
      gates[srcID]->out = (smpc_wire*)malloc(sizeof(smpc_wire));
      gates[srcID]->out->back = gates[srcID];
      gates[srcID]->out->forward = smpc_evgatelist_create();
    }
    smpc_evgatelist_insert(gates[srcID]->out->forward, gates[dstID]);
    if (gates[dstID]->in0 == NULL) gates[dstID]->in0 = gates[srcID]->out;
    else if (gates[dstID]->in1 == NULL) gates[dstID]->in1 = gates[srcID]->out;
    else return SMPC_IO_ERROR_PARSE_WIRES_TOO_MANY_GATE_INPUT_LINES;
    _smpc_io_readline(line,f);
    ++i;
  }
  return SMPC_SUCCESS;
}

int _smpc_io_parse_graph_output_section(smpc_graph** graph,smpc_evgate** gates,
                                       int gatesCount,FILE* f, char* line) {
  _smpc_io_readline(line,f);
  while(!feof(f)) {
    int index = strtoul(line, NULL, 10);
    if (errno == EINVAL) return SMPC_IO_ERROR_PARSE_OUTPUT_INVALID_NUMBER;
    if (index < 0 || index >= gatesCount)
      return SMPC_IO_ERROR_PARSE_OUTPUT_OUT_OF_RANGE;
    smpc_wire* wire = (smpc_wire*)malloc(sizeof(smpc_wire));
    wire->back = gates[index];
    wire->forward = NULL;
    smpc_wirelist_insert((*graph)->output_wires,wire);
    gates[index]->out = wire;
    _smpc_io_readline(line,f);
  }
  return SMPC_SUCCESS;
}

int smpc_io_parse_graph_from_file(smpc_graph** graph, char* filename) {
  if (graph == NULL) return SMPC_IO_ERROR_PARSE_INVALID_GRAPH_POINTER;
  if (*graph == NULL){
    *graph = (smpc_graph*)malloc(sizeof(smpc_graph));
    (*graph)->input_wires = smpc_wirelist_create();
    (*graph)->output_wires = smpc_wirelist_create();
    (*graph)->sorted_gates = smpc_evgatelist_create();
    (*graph)->options = 0;
  }
  FILE* f = fopen(filename, "r");
  int errorCode;
  if (!f) return SMPC_IO_ERROR_FILENOTFOUND;

  char lineBuffer[SMPC_IO_PARSE_LINE_BUFFER_SIZE];

  _smpc_io_readline(lineBuffer,f);
  if (!_smpc_io_string_starts_with(lineBuffer,"Gates"))
    return SMPC_IO_ERROR_PARSE_GATES_EXPECTED;
  char* strp = lineBuffer + 6;
  while (*strp == ' ') ++strp;
  int gatesCount = strtoul(strp, NULL, 10);
  if (errno == EINVAL) return SMPC_IO_ERROR_PARSE_GATES_INVALID_LENGTH;
  smpc_evgate** gates = malloc(sizeof(smpc_evgate) * gatesCount);
  errorCode = _smpc_io_parse_graph_gates_section(graph,gates,gatesCount,f,lineBuffer);
  if (SMPC_FAIL(errorCode)) return errorCode;

  _smpc_io_readline(lineBuffer,f);
  if (!_smpc_io_string_starts_with(lineBuffer,"Input"))
    return SMPC_IO_ERROR_PARSE_INPUT_EXPECTED;
  errorCode = _smpc_io_parse_graph_input_section(graph,gates,gatesCount,
                                                 f,lineBuffer);
  if (SMPC_FAIL(errorCode)) return errorCode;
  errorCode = _smpc_io_parse_graph_wires_section(graph,gates,gatesCount,
                                                 f,lineBuffer);
  if (SMPC_FAIL(errorCode)) return errorCode;
  errorCode = _smpc_io_parse_graph_output_section(graph,gates,gatesCount,
                                                 f,lineBuffer);
  if (SMPC_FAIL(errorCode)) return errorCode;

  free(gates);
  fclose(f);
  return SMPC_SUCCESS;
}

void _smpc_io_print_wirelist(smpc_io_wirelist* wireList) {
  smpc_io_wirelist_node* n = wireList->head;
  for (size_t i = 0; i < wireList->length; i++) {
    printf("back: %d ", n->back_gate);
    printf("forward: %d ", n->forward_gate);
    printf("in %d\n", n->in);
    n = n->next;
  }
}

void _smpc_io_print_gate_array(smpc_io_gate* gateArray, int numberOfGates) {
  for (size_t i = 0; i < numberOfGates; i++) {
    printf("Gate: %ld ", i);
    switch (gateArray[i].type) {
      case SMPC_EVGATE_TYPE_AND: {
        printf("AND ");
        break;
      }
      case SMPC_EVGATE_TYPE_OR: {
        printf("OR ");
        break;
      }
      case SMPC_EVGATE_TYPE_NAND: {
        printf("NAND ");
        break;
      }
      case SMPC_EVGATE_TYPE_NOR: {
        printf("NOR ");
        break;
      }
      case SMPC_EVGATE_TYPE_XOR: {
        printf("XOR ");
        break;
      }
      case SMPC_EVGATE_TYPE_XNOR: {
        printf("XNOR ");
        break;
      }
      case SMPC_EVGATE_TYPE_NOT_A: {
        printf("NOT ");
        break;
      }
      default: {
        break;
      }
    }
    if (gateArray[i].oflag != 0) {
      printf("OUTPUT");
    }
    printf("\n");
    _smpc_io_print_wirelist(gateArray[i].wire_list);
    printf("\n");
  }
}

smpc_io_wirelist* _smpc_io_wirelist_create() {
  smpc_io_wirelist* a = (smpc_io_wirelist*) malloc(sizeof(smpc_io_wirelist));
  a->head = NULL;
  a->tail = NULL;
  a->length = 0;
  return a;
}

void _smpc_io_wirelist_insert(smpc_io_wirelist* list, int back, int forward, int in) {
  smpc_io_wirelist_node* n = (smpc_io_wirelist_node*)malloc(sizeof(smpc_io_wirelist_node));
  n->back_gate = back;
  n->forward_gate = forward;
  n->in = in;
  if(list->head == NULL) list->head = n;
  n->prev = list->tail;
  n->next = NULL;
  list->tail = n;
  if (n->prev != NULL) n->prev->next = n;
  list->length ++;
}

int _smpc_io_parse_graph_cbmc_file_gate(smpc_graph** graph, smpc_io_gate* gateArray,
                                        int numberOfGates, FILE* f, char* line) {
  int type;
  int gateIndex;
  int in;
  char* endOfIndex;
  char* newEndOfIndex;
  smpc_io_wirelist* wireList;
  for (size_t i = 0; i < numberOfGates; i++) {
    _smpc_io_readline(line, f);
    endOfIndex = line;
    if (_smpc_io_string_starts_with(endOfIndex,"AND")) {type = SMPC_EVGATE_TYPE_AND; endOfIndex += 3;}
    else if (_smpc_io_string_starts_with(endOfIndex,"OR")) {type = SMPC_EVGATE_TYPE_OR; endOfIndex += 2;}
    else if (_smpc_io_string_starts_with(endOfIndex,"NAND")) {type = SMPC_EVGATE_TYPE_NAND; endOfIndex += 4;}
    else if (_smpc_io_string_starts_with(endOfIndex,"NOR")) {type = SMPC_EVGATE_TYPE_NOR; endOfIndex += 3;}
    else if (_smpc_io_string_starts_with(endOfIndex,"XOR")) {type = SMPC_EVGATE_TYPE_XOR; endOfIndex += 3;}
    else if (_smpc_io_string_starts_with(endOfIndex,"XNOR")) {type = SMPC_EVGATE_TYPE_XNOR; endOfIndex += 4;}
    else if (_smpc_io_string_starts_with(endOfIndex,"NOT")) {type = SMPC_EVGATE_TYPE_NOT_A; endOfIndex += 3;}
    else return SMPC_IO_ERROR_PARSE_GATES_INVALID_TYPE;
    endOfIndex +=  2;
    wireList = _smpc_io_wirelist_create();
    gateArray[i].id = i+1;
    gateArray[i].type = type;
    gateArray[i].oflag = 0;
    while (*endOfIndex != '\n') {
      endOfIndex += 3;
      if (*endOfIndex == '-') {
        while (*endOfIndex != ':') {
          endOfIndex ++;
        }
        gateArray[i].oflag ++;
        endOfIndex += 2;
      }
      else {
        gateIndex = strtoul(endOfIndex, &newEndOfIndex, 10);
        endOfIndex = newEndOfIndex;
        endOfIndex++;
        in = strtoul(endOfIndex, &newEndOfIndex, 10);
        endOfIndex = newEndOfIndex;
        _smpc_io_wirelist_insert(wireList, i, gateIndex - 1, in);
      }
    }
    if (gateArray[i].type != SMPC_EVGATE_TYPE_NOT_A) {
      gateArray[i].key = (smpc_evgate*)malloc(sizeof(smpc_evgate));
      memset(gateArray[i].key->lut,0,sizeof(gateArray[i].key->lut));
      gateArray[i].key->in0 = NULL;
      gateArray[i].key->in1 = NULL;
      gateArray[i].key->out = NULL;
      gateArray[i].key->visited = false;
      gateArray[i].key->id = 0;
      gateArray[i].key->not = SMPC_NOT_NONE;
      gateArray[i].key->type = gateArray[i].type;
    }
    gateArray[i].wire_list = wireList;
  }
  return 0;
}

void _smpc_io_not_gate_type(smpc_io_gate* gateArray, size_t index) {
  switch (gateArray[index].type) {
    case SMPC_EVGATE_TYPE_AND: {
      gateArray[index].type = SMPC_EVGATE_TYPE_NAND;
      gateArray[index].key->type = SMPC_EVGATE_TYPE_NAND;
      break;
    }
    case SMPC_EVGATE_TYPE_OR: {
      gateArray[index].type = SMPC_EVGATE_TYPE_NOR;
      gateArray[index].key->type = SMPC_EVGATE_TYPE_NOR;
      break;
    }
    case SMPC_EVGATE_TYPE_NAND: {
      gateArray[index].type = SMPC_EVGATE_TYPE_AND;
      gateArray[index].key->type = SMPC_EVGATE_TYPE_AND;
      break;
    }
    case SMPC_EVGATE_TYPE_NOR: {
      gateArray[index].type = SMPC_EVGATE_TYPE_OR;
      gateArray[index].key->type = SMPC_EVGATE_TYPE_OR;
      break;
    }
    case SMPC_EVGATE_TYPE_XOR: {
      gateArray[index].type = SMPC_EVGATE_TYPE_XNOR;
      gateArray[index].key->type = SMPC_EVGATE_TYPE_XNOR;
      break;
    }
    case SMPC_EVGATE_TYPE_XNOR: {
      gateArray[index].type = SMPC_EVGATE_TYPE_XOR;
      gateArray[index].key->type = SMPC_EVGATE_TYPE_XOR;
      break;
    }
    default:{
      break;
    }
  }
}

void _smpc_io_parse_graph_gates_assemble(smpc_graph** graph, smpc_io_gate* gateArray,
                                         int numberOfGates, size_t skipIndex) {
  size_t j;
  for (size_t i = 0; i < numberOfGates; i++) {
    if (i != skipIndex) {
      if (gateArray[i].type != SMPC_EVGATE_TYPE_NOT_A && gateArray[i].type != SMPC_EVGATE_TYPE_NOT_B) {
        smpc_io_wirelist_node* node = gateArray[i].wire_list->head;
        gateArray[i].key->out = (smpc_wire*)malloc(sizeof(smpc_wire));
        gateArray[i].key->out->state = false;
        memset(gateArray[i].key->out->v, 0, sizeof(gateArray[i].key->out->v));
        memset(gateArray[i].key->out->k0, 0, sizeof(gateArray[i].key->out->k0));
        memset(gateArray[i].key->out->k1, 0, sizeof(gateArray[i].key->out->k1));
        gateArray[i].key->out->back = gateArray[i].key;
        if (gateArray[i].wire_list->length == 0) {
          smpc_wirelist_insert((*graph)->output_wires, gateArray[i].key->out);
          gateArray[i].key->out->forward = NULL;
        }
        else {
          gateArray[i].key->out->forward = smpc_evgatelist_create();
        }
        for (size_t k = 0; k < gateArray[i].wire_list->length; k++) {
          if (gateArray[node->forward_gate].type == SMPC_EVGATE_TYPE_NOT_A) {
            if (gateArray[i].wire_list->length == 1) {
              gateArray[node->forward_gate].type = SMPC_EVGATE_TYPE_NOT_B;
              _smpc_io_not_gate_type(gateArray, i);
              if (gateArray[node->forward_gate].wire_list->length == 0) {
                smpc_wirelist_insert((*graph)->output_wires, gateArray[i].key->out);
                free(gateArray[i].key->out->forward);
                gateArray[i].key->out->forward = NULL;
              }
            }
            smpc_io_wirelist_node* wireNode = gateArray[node->forward_gate].wire_list->head;
            for (size_t z = 0; z < gateArray[node->forward_gate].wire_list->length; z++) {
              _smpc_io_wirelist_insert(gateArray[i].wire_list, i, wireNode->forward_gate, wireNode->in);
              wireNode = wireNode->next;
            }
          }
          else {
            smpc_evgatelist_insert(gateArray[i].key->out->forward, gateArray[node->forward_gate].key);
            if (node->in == 0) gateArray[node->forward_gate].key->in0 = gateArray[i].key->out;
            if (node->in == 1) gateArray[node->forward_gate].key->in1 = gateArray[i].key->out;
          }
          node = node->next;
        }
      }
      else {
        if (gateArray[i].type == SMPC_EVGATE_TYPE_NOT_B) {

        }
        else {
          smpc_io_wirelist_node* nodeB = gateArray[i].wire_list->head;
          for (size_t k = 0; k < gateArray[i].wire_list->length; k++) {
            if (nodeB->in == 0){
              if (gateArray[nodeB->forward_gate].key->not == SMPC_NOT_NONE) {
                gateArray[nodeB->forward_gate].key->not = SMPC_NOT_A;
              }
              else {
                gateArray[nodeB->forward_gate].key->not = SMPC_NOT_E;
              }
            }
            if (nodeB->in == 1) {
              if (gateArray[nodeB->forward_gate].key->not == SMPC_NOT_NONE) {
                gateArray[nodeB->forward_gate].key->not = SMPC_NOT_B;
              }
              else {
                gateArray[nodeB->forward_gate].key->not = SMPC_NOT_E;
              }
            }
            nodeB = nodeB->next;
          }
        }
      }
    }
  }
}

void _smpc_io_parse_graph_cbmc_file_input(smpc_graph** graph, smpc_io_gate* gateArray,
                                         FILE* f, char* line) {
  int index;
  int in;
  char* lcIndex;
  char* newlcIndex;
  smpc_wire* wire;
  int z = 0;
  while (_smpc_io_readline(line, f) == 0) {
    if (strlen(line) == 0) {
      break;
    }
    z++;
    lcIndex = line;
    lcIndex += 8;
    index = strtoul(lcIndex, &newlcIndex, 10);
    lcIndex = newlcIndex;
    wire = (smpc_wire*)malloc(sizeof(smpc_wire));
    wire->back = NULL;
    wire->forward = smpc_evgatelist_create();
    memset(wire->v, 0, sizeof(wire->v));
    memset(wire->k0, 0, sizeof(wire->k0));
    memset(wire->k1, 0, sizeof(wire->k1));
    wire->state = false;
    smpc_wirelist_insert((*graph)->input_wires, wire);
    while (*lcIndex != '\n') {
      lcIndex += 3;
      index = strtoul(lcIndex, &newlcIndex, 10);
      lcIndex = newlcIndex;
      lcIndex ++;
      index --;
      in = strtoul(lcIndex, &newlcIndex, 10);
      lcIndex = newlcIndex;
      if (gateArray[index].type == SMPC_EVGATE_TYPE_NOT_A) {
        smpc_io_wirelist_node* wn = gateArray[index].wire_list->head;
        for (size_t q = 0; q < gateArray[index].wire_list->length; q++) {
          smpc_evgatelist_insert(wire->forward, gateArray[wn->forward_gate].key);
          if (wn->in == 0) gateArray[wn->forward_gate].key->in0 = wire;
          if (wn->in == 1) gateArray[wn->forward_gate].key->in1 = wire;
          wn = wn->next;
        }
      }
      else {
        smpc_evgatelist_insert(wire->forward, gateArray[index].key);
        if(in == 0) gateArray[index].key->in0 = wire;
        if(in == 1) gateArray[index].key->in1 = wire;
      }
    }
    line[0] = '\0';
  }
}

int smpc_io_parse_graph_from_cbmc_gc(smpc_graph** graph, char* dirname, int verb) {
  if (graph == NULL) return SMPC_IO_ERROR_PARSE_INVALID_GRAPH_POINTER;
  if (*graph == NULL){
    *graph = (smpc_graph*)malloc(sizeof(smpc_graph));
    (*graph)->input_wires = smpc_wirelist_create();
    (*graph)->output_wires = smpc_wirelist_create();
    (*graph)->sorted_gates = smpc_evgatelist_create();
    (*graph)->options = 0;

    int dirNameLen = strlen(dirname);
    char path[512];
    path[0] = '\0';
    strcat(path, dirname);
    strcat(path, SMPC_IO_PATH_NAME_NOG);
    FILE* f = fopen(path, "r");
    int errorCode;
    if (!f) return SMPC_IO_ERROR_FILENOTFOUND;

    char lineBuffer[SMPC_IO_PARSE_LINE_BUFFER_SIZE];

    _smpc_io_readline(lineBuffer,f);
    if (strlen(lineBuffer) == 0) return SMPC_IO_ERROR_PARSE_NOG_EXPECTED;
    int numberOfGates;
    char* endOfNumber;
    numberOfGates = strtoul(lineBuffer, &endOfNumber, 10);
    fclose(f);
    smpc_io_gate gateArray[numberOfGates];
    path[0] = '\0';
    strcat(path, dirname);
    strcat(path, SMPC_IO_PATH_NAME_GATE);
    f = fopen(path, "r");
    if (!f) return SMPC_IO_ERROR_FILENOTFOUND;
    errorCode = _smpc_io_parse_graph_cbmc_file_gate(graph, gateArray, numberOfGates,
                                                    f, lineBuffer);
    fclose(f);
    if (SMPC_FAIL(errorCode)) return errorCode;
    if (verb == 1) _smpc_io_print_gate_array(gateArray, numberOfGates);
    path[0] = '\0';
    strcat(path, dirname);
    strcat(path, SMPC_IO_PATH_NAME_CONSTANTS);
    f = fopen(path, "r");
    if (!f) return SMPC_IO_ERROR_FILENOTFOUND;
    fseek(f,0, SEEK_END);
    unsigned long len = (unsigned long)ftell(f);
    size_t skipIndex = -1;
    if (len > 0) {
      rewind(f);
      _smpc_io_readline(lineBuffer, f);
      if (strlen(lineBuffer) != 0) {
        char* eoi = lineBuffer;
        while (*eoi != ':') eoi++;
        eoi++;
        skipIndex = strtoul(eoi, NULL, 10);
        skipIndex--;
      }
    }
    fclose(f);
    _smpc_io_parse_graph_gates_assemble(graph, gateArray, numberOfGates, skipIndex);
    path[0] = '\0';
    strcat(path, dirname);
    strcat(path, SMPC_IO_PATH_NAME_INPUTS);
    f = fopen(path, "r");
    if (!f) return SMPC_IO_ERROR_FILENOTFOUND;
    _smpc_io_parse_graph_cbmc_file_input(graph, gateArray, f, lineBuffer);
    fclose(f);
    return 0;
  }
}

#ifdef INCLUDE_BAD_CODE
void _smpc_io_print_graph_recursive(smpc_wire* wire, int tabLevel, FILE* f) {
  if (tabLevel >= 255 || !wire) return;
  char tabs[256]; memset(tabs,'\t',256*sizeof(char));
  tabs[tabLevel] = 0;
  printf("%sWire %016lX\n",tabs,wire);
  printf("%s\tBack   : 0x%016lX\n", tabs, wire->back);
  printf("%s\tForward: \n", tabs);
  if (!wire->forward) return;
  smpc_evgatelist_node* n = wire->forward->head;
  while (n != NULL) {
    printf("%s\t\tEVGATE 0x%016lX (0x%016lX,0x%016lX)\n",tabs, n->key,n->key->in0,n->key->in1);
    _smpc_io_print_graph_recursive(n->key->out,tabLevel + 2,f);
    n = n->next;
  }
}

void smpc_io_print_graph(smpc_graph* graph, FILE* f) {
  smpc_wirelist_node* p = graph->input_wires->head;
  while (p != NULL) {
    printf("Initial Wire 0x%016lX\n", p->key);
    printf("\tBack   : 0x%016lX\n", p->key->back);
    printf("\tForward: \n");
    smpc_evgatelist_node* n = p->key->forward->head;
    while (n != NULL) {
      printf("\t\tEVGATE 0x%016lX (0x%016lX,0x%016lX)\n", n->key,n->key->in0,n->key->in1);
      if (!n->key) break;
      _smpc_io_print_graph_recursive(n->key->out,2,f);
      n = n->next;
    }
    p = p->next;
  }
}

#endif
