#include <stdio.h>
#include "smpc.h"
#include "lib-misc.h"
#include "bitut.h"
#include <nettle/sha3.h>
#include <nettle/yarrow.h>
#include "string.h"

struct yarrow256_ctx SMPC_PRNG;

int smpc_initialize() {
  long int seed = 0;
  yarrow256_init(&SMPC_PRNG, 0, NULL);
  if (SMPC_FAIL(extract_randseed_os_rng((uint8_t *)&seed, sizeof(seed))))
    return -1;
  yarrow256_seed(&SMPC_PRNG, sizeof(seed), (uint8_t *)&seed);
  return SMPC_SUCCESS;
}

smpc_evgatelist* smpc_evgatelist_create() {
  smpc_evgatelist* a = (smpc_evgatelist*) malloc(sizeof(smpc_evgatelist));
  a->head = NULL;
  a->tail = NULL;
  a->length = 0;
  return a;
}
smpc_wirelist* smpc_wirelist_create() {
  smpc_wirelist* a = (smpc_wirelist*) malloc(sizeof(smpc_wirelist));
  a->head = NULL;
  a->tail = NULL;
  a->length = 0;
  return a;
}

void smpc_evgatelist_insert(smpc_evgatelist* list, smpc_evgate* key) {
  smpc_evgatelist_node* n = (smpc_evgatelist_node*)malloc(sizeof(smpc_evgatelist_node));
  n->key = key;
  if(list->head == NULL)
    list->head = n;
  n->prev = list->tail;
  list->tail = n;
  n->next = NULL;
  if (n->prev != NULL) n->prev->next = n;
  list->length ++;
}

void smpc_wirelist_insert(smpc_wirelist* list, smpc_wire* key) {
  smpc_wirelist_node* n=(smpc_wirelist_node*)malloc(sizeof(smpc_wirelist_node));
  if(key == NULL) printf("%ld, NULL\n", list->length);
  n->key = key;
  if(list->head == NULL)
    list->head = n;
  n->prev = list->tail;
  list->tail = n;
  n->next = NULL;
  if (n->prev != NULL) n->prev->next = n;
  list->length ++;
}

void smpc_topological_sort_visit(smpc_graph* graph, smpc_evgate* gate, unsigned int* i) {
  if (!gate->visited) {
      gate->visited = true;
      if (gate->out && gate->out->forward) {
        smpc_evgatelist_node* p = gate->out->forward->head;
        while (p) {
          smpc_topological_sort_visit(graph,p->key, i);
          p = p->next;
        }
      }
      gate->id = *i + 1;
      *i = *i + 1;
      smpc_evgatelist_insert(graph->sorted_gates,gate);
  }
}

void smpc_topological_sort(smpc_graph* g) {
  unsigned int i = 0;
  smpc_wirelist_node* inputWire = g->input_wires->head;
  while (inputWire){
    smpc_evgatelist_node* currentGate = inputWire->key->forward->head;
    while (currentGate) {
      smpc_topological_sort_visit(g,currentGate->key, &i);
      currentGate = currentGate->next;
    }
    inputWire = inputWire->next;
  }
}

void smpc_evaluate_offline(smpc_graph* g, uint8_t* input, uint8_t* output) {
  if (g->sorted_gates->length == 0)
    smpc_topological_sort(g);
  smpc_evgatelist_node* p = g->sorted_gates->tail;
  smpc_wirelist_node* wireNode = g->input_wires->head;
  for (size_t i = 0; i < g->input_wires->length; i++) {
    wireNode->key->v[0] = (input[i/8] >> ((i % 8))) & 0x1;
    wireNode = wireNode->next;
  }
  while (p != NULL) {
    switch (p->key->type) {
      case SMPC_EVGATE_TYPE_AND: {
        p->key->out->v[0] = p->key->in0->v[0] & p->key->in1->v[0];
        break;
      }
      case SMPC_EVGATE_TYPE_OR: {
        p->key->out->v[0] = p->key->in0->v[0] | p->key->in1->v[0];
        break;
      }
      case SMPC_EVGATE_TYPE_NAND: {
        p->key->out->v[0] = 1-(p->key->in0->v[0] & p->key->in1->v[0]);
        break;
      }
      case SMPC_EVGATE_TYPE_NOR: {
        p->key->out->v[0] = 1-(p->key->in0->v[0] | p->key->in1->v[0]);
        break;
      }
      case SMPC_EVGATE_TYPE_XOR: {
        p->key->out->v[0] = p->key->in0->v[0] ^ p->key->in1->v[0];
        break;
      }
      case SMPC_EVGATE_TYPE_XNOR: {
        p->key->out->v[0] = ~(p->key->in0->v[0] ^ p->key->in1->v[0]);
        break;
      }
      default: {
        break;
      }
    }
    p = p->prev;
  }

  wireNode = g->output_wires->head;
  for (size_t i = 0; i < g->output_wires->length; i++) {
    output[i/8] |= (wireNode->key->v[0] << (i % 8));
    wireNode = wireNode->next;
  }
}


void _smpc_copy_string_bh(uint8_t* c, uint8_t* a, uint8_t* b, unsigned int id) {
  memcpy(c,a, SMPC_KEY_B_NUM * sizeof(uint8_t));
  memcpy(c + SMPC_KEY_B_NUM, b, SMPC_KEY_B_NUM * sizeof(uint8_t));
  memcpy(c + (SMPC_KEY_B_NUM*2), (uint8_t*)&id, 2);
}

void _smpc_set_lut_pp(smpc_evgatelist_node* p, uint8_t p0, uint8_t p1, uint8_t r){
  if (p0 == 0) {
    ClearBitU(p->key->lut[r][SMPC_KEY_B_NUM], 0);
  }
  else {
    SetBitU(p->key->lut[r][SMPC_KEY_B_NUM], 0);
  }
  if (p1 == 0) {
    ClearBitU(p->key->lut[r][SMPC_KEY_B_NUM], 1);
  }
  else {
    SetBitU(p->key->lut[r][SMPC_KEY_B_NUM], 1);
  }
}

void _smpc_copy_string_into_lut_pp(uint8_t* a, uint8_t* b, uint8_t p0, uint8_t p1) {
  for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
    a[i] = b[i];
  }
  if (p0 == 0) {
    ClearBitU(a[SMPC_KEY_B_NUM], 0);
  }
  else {
    SetBitU(a[SMPC_KEY_B_NUM], 0);
  }
  if (p1 == 0) {
    ClearBitU(a[SMPC_KEY_B_NUM], 1);
  }
  else {
    SetBitU(a[SMPC_KEY_B_NUM], 1);
  }
}

uint8_t _smpc_test_bit(uint8_t* s, uint8_t i) {
  switch (i) {
    case SMPC_KEY_POINTER:{
      if (TestBit(s, SMPC_KEY_POINTER)) return 1;
      else return 0;
      break;
    }
    case 0:{
      if (TestBitU(*s, 0)) return 1;
      else return 0;
      break;
    }
    case 1:{
      if (TestBitU(*s, 1)) return 1;
      else return 0;
      break;
    }
    default:{
      if (TestBitU(*s, i)) return 1;
      else return 0;
    }
  }
}

void _smpc_xor(uint8_t* output, uint8_t* k, uint8_t* delta){
  for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
    output[i] = k[i] ^ delta[i];
  }
}

void _smpc_set_lut_row(smpc_evgatelist_node* p, uint8_t b0, uint8_t b1, uint8_t o, uint8_t r) {
  uint8_t *x, *y, *z;
  if (b0 == 0) {
    x = p->key->in0->k0;
  }
  else {
    x = p->key->in0->k1;
  }
  if (b1 == 0) {
    y = p->key->in1->k0;
  }
  else {
    y = p->key->in1->k1;
  }
  if (o == 0) {
    z = p->key->out->k0;
  }
  else {
    z = p->key->out->k1;
  }
  uint8_t tk[(SMPC_KEY_B_NUM*2) + 2];
  _smpc_copy_string_bh(tk, x, y, p->key->id);
  struct sha3_256_ctx context;
  sha3_256_init(&context);
  sha3_256_update (&context, (SMPC_KEY_B_NUM*2) + 2, tk);
  uint8_t digest[SMPC_KEY_B_NUM];
  sha3_256_digest(&context, SMPC_KEY_B_NUM, digest);
  uint8_t record[SMPC_KEY_B_NUM];
  for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
    record[i] = digest[i] ^ z[i];
  }
  _smpc_copy_string_into_lut_pp(p->key->lut[r], record,
                               _smpc_test_bit(x, SMPC_KEY_POINTER),
                               _smpc_test_bit(y, SMPC_KEY_POINTER)
                              );
}


int _smpc_lut_pp_metch(smpc_evgatelist_node* p, uint8_t* x, uint8_t* y, uint8_t r) {
  if ((_smpc_test_bit(&p->key->lut[r][SMPC_KEY_B_NUM], 0)
        ==
       _smpc_test_bit(x, SMPC_KEY_POINTER))
       &&
      (_smpc_test_bit(&p->key->lut[r][SMPC_KEY_B_NUM], 1)
        ==
       _smpc_test_bit(y, SMPC_KEY_POINTER)))
    return 1;

  else
    return 0;
}

void _smpc_set_grr3_zero_row(smpc_evgatelist_node* p, int* tv) {
  uint8_t tk[(SMPC_KEY_B_NUM*2) + 2];
  uint8_t index = 0;
  if(_smpc_test_bit(p->key->in1->k1, SMPC_KEY_POINTER) == 0)
    index = 1;
  if (_smpc_test_bit(p->key->in0->k1, SMPC_KEY_POINTER) == 0)
    index += 2;
  uint8_t *a, *b;
  switch (index) {
    case 0:{
      _smpc_copy_string_bh(tk, p->key->in0->k0, p->key->in1->k0, p->key->id);
      a = p->key->in0->k0;
      b = p->key->in1->k0;
      break;
    }
    case 1:{
      _smpc_copy_string_bh(tk, p->key->in0->k0, p->key->in1->k1, p->key->id);
      a = p->key->in0->k0;
      b = p->key->in1->k1;
      break;
    }
    case 2:{
      _smpc_copy_string_bh(tk, p->key->in0->k1, p->key->in1->k0, p->key->id);
      a = p->key->in0->k1;
      b = p->key->in1->k0;
      break;
    }
    case 3:{
      _smpc_copy_string_bh(tk, p->key->in0->k1, p->key->in1->k1, p->key->id);
      a = p->key->in0->k1;
      b = p->key->in1->k1;
      break;
    }
  }
  uint8_t* theChosenOne;
  uint8_t* theOtherOne;
  if (tv[index] == 0) {
    theChosenOne = p->key->out->k0;
    theOtherOne = p->key->out->k1;
  }
  else {
    theChosenOne = p->key->out->k1;
    theOtherOne = p->key->out->k0;
  }
  ClearAll(p->key->lut[0], SMPC_KEY_B_NUM);
  struct sha3_256_ctx context;
  sha3_256_init (&context);
  sha3_256_update (&context, (SMPC_KEY_B_NUM*2) + 2, tk);
  sha3_256_digest(&context, SMPC_KEY_B_NUM, theChosenOne);
  _smpc_set_lut_pp(p, _smpc_test_bit(a, SMPC_KEY_POINTER),
                   _smpc_test_bit(b, SMPC_KEY_POINTER),
                   0
                  );
  yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, theOtherOne);
  if (_smpc_test_bit(theChosenOne, SMPC_KEY_POINTER) == 0) {
    SetBit(theOtherOne, SMPC_KEY_POINTER);
  }
  else {
    ClearBit(theOtherOne, SMPC_KEY_POINTER);
  }
}

void _smpc_set_grr3_other_row(smpc_evgatelist_node* p, int* tv) {
  size_t j = 0;
  uint8_t x, y, z;
  if(_smpc_test_bit(p->key->in1->k1, SMPC_KEY_POINTER) == 0)
    j = 1;
  if (_smpc_test_bit(p->key->in0->k1, SMPC_KEY_POINTER) == 0)
    j += 2;
  switch (j) {
    case 0:{
      _smpc_set_lut_row(p, 0, 1, tv[1], 1);
      _smpc_set_lut_row(p, 1, 0, tv[2], 2);
      _smpc_set_lut_row(p, 1, 1, tv[3], 3);
      break;
    }
    case 1:{
      _smpc_set_lut_row(p, 0, 0, tv[0], 1);
      _smpc_set_lut_row(p, 1, 1, tv[3], 2);
      _smpc_set_lut_row(p, 1, 0, tv[2], 3);
      break;
    }
    case 2:{
      _smpc_set_lut_row(p, 1, 1, tv[3], 1);
      _smpc_set_lut_row(p, 0, 0, tv[0], 2);
      _smpc_set_lut_row(p, 0, 1, tv[1], 3);
      break;
    }
    case 3:{
      _smpc_set_lut_row(p, 1, 0, tv[2], 1);
      _smpc_set_lut_row(p, 0, 1, tv[1], 2);
      _smpc_set_lut_row(p, 0, 0, tv[0], 3);
      break;
    }
  }
}

void _smpc_exchange_lut_rows(smpc_evgatelist_node* p, uint8_t a, uint8_t b){
  uint8_t tmp[SMPC_LUT_B_NUM];
  memcpy(tmp, p->key->lut[a], SMPC_LUT_B_NUM * sizeof(uint8_t));
  memcpy(p->key->lut[a], p->key->lut[b], SMPC_LUT_B_NUM * sizeof(uint8_t));
  memcpy(p->key->lut[b], tmp, SMPC_LUT_B_NUM * sizeof(uint8_t));
}

void _smpc_permute_grr3_lut_row(smpc_evgatelist_node* p) {
  uint8_t prn[SMPC_KEY_B_NUM];
  int j = 0;
  yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, prn);
  for (uint8_t i = 1; i < 4; i++) {
    j = (prn[i] % 3) + 1;
    if (i != j) {
      _smpc_exchange_lut_rows(p, i, j);
    }
  }
}

void _smpc_permute_lut_row(smpc_evgatelist_node* p) {
  uint8_t prn[SMPC_KEY_B_NUM];
  int j = 0;
  yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, prn);
  for (uint8_t i = 0; i < 4; i++) {
    j = (prn[i] % 4);
    if (i != j) {
      _smpc_exchange_lut_rows(p, i, j);
    }
  }
}

void _smpc_set_free_xor_gate(smpc_evgatelist_node* p, uint8_t* delta) {
  uint8_t* ka = p->key->in0->k0;
  uint8_t* kb = p->key->in1->k0;
  uint8_t* za = p->key->out->k0;
  uint8_t* zb = p->key->out->k1;
  if (p->key->type == SMPC_EVGATE_TYPE_XNOR) {
    za = p->key->out->k1;
    zb = p->key->out->k0;
  }
  for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
    za[i] = ka[i] ^ kb[i];
  }
  if (_smpc_test_bit(za, SMPC_KEY_POINTER) == 0)
    SetBit(zb, SMPC_KEY_POINTER);
  else
    ClearBit(zb, SMPC_KEY_POINTER);
  _smpc_xor(zb, za, delta);
}

void _smpc_set_gate_lut_keys(smpc_evgatelist_node* p, int* tv) {
  uint8_t key[SMPC_KEY_B_NUM];
  yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, key);
  memcpy(p->key->out->k0, key, SMPC_KEY_B_NUM * sizeof(uint8_t));
  yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, key);
  memcpy(p->key->out->k1, key, SMPC_KEY_B_NUM * sizeof(uint8_t));
  if (_smpc_test_bit(p->key->out->k0, SMPC_KEY_POINTER))
    ClearBit(p->key->out->k1, SMPC_KEY_POINTER);
  else
    SetBit(p->key->out->k1, SMPC_KEY_POINTER);
  uint8_t x, y, z;
  uint8_t *a, *b;
  for (size_t i = 0; i < 4; i++) {
    switch (i) {
      case 0:{
        a = p->key->in0->k0;
        b = p->key->in1->k0;
        x = 0;
        y = 0;
        break;
      }
      case 1:{
        a = p->key->in0->k0;
        b = p->key->in1->k1;
        x = 0;
        y = 1;
        break;
      }
      case 2:{
        a = p->key->in0->k1;
        b = p->key->in1->k0;
        x = 1;
        y = 0;
        break;
      }
      case 3:{
        a = p->key->in0->k1;
        b = p->key->in1->k1;
        x = 1;
        y = 1;
        break;
      }
    }
    z = tv[i];
    _smpc_set_lut_row(p, x, y, z, i);
  }
  _smpc_permute_lut_row(p);
}

void _smpc_set_grr3_lut_row(smpc_evgatelist_node* p, int* tv) {
  _smpc_set_grr3_zero_row(p, tv);
  _smpc_set_grr3_other_row(p, tv);
//  _smpc_permute_grr3_lut_row(p);
}

void _smpc_set_end_lut_row(smpc_evgatelist_node* p, int* tv) {
  uint8_t tk[(SMPC_KEY_B_NUM*2) + 2];
  uint8_t x, y, z;
  uint8_t *a, *b;
  ClearAll(p->key->out->k0, SMPC_KEY_B_NUM);
  ClearAll(p->key->out->k1, SMPC_KEY_B_NUM);

  SetBit(p->key->out->k1, 0);
  for (size_t i = 0; i < 4; i++) {
    switch (i) {
      case 0:{
        _smpc_copy_string_bh(tk, p->key->in0->k0, p->key->in1->k0, p->key->id);
        a = p->key->in0->k0;
        b = p->key->in1->k0;
        x = 0;
        y = 0;
        break;
      }
      case 1:{
        _smpc_copy_string_bh(tk, p->key->in0->k0, p->key->in1->k1, p->key->id);
        a = p->key->in0->k0;
        b = p->key->in1->k1;
        x = 0;
        y = 1;
        break;
      }
      case 2:{
        _smpc_copy_string_bh(tk, p->key->in0->k1, p->key->in1->k0, p->key->id);
        a = p->key->in0->k1;
        b = p->key->in1->k0;
        x = 1;
        y = 0;
        break;
      }
      case 3:{
        _smpc_copy_string_bh(tk, p->key->in0->k1, p->key->in1->k1, p->key->id);
        a = p->key->in0->k1;
        b = p->key->in1->k1;
        x = 1;
        y = 1;
        break;
      }
    }
    z = tv[i];
    _smpc_set_lut_row(p, x, y, z, i);
  }
  _smpc_permute_lut_row(p);
}

int _smpc_is_output(smpc_graph* g, smpc_evgatelist_node* p) {
  smpc_wirelist_node* t = g->output_wires->head;
  for (size_t i = 0; i < g->output_wires->length; i++) {
    if (p->key->out == t->key) {
      return 1;
    }
    t = t->next;
  }
  return 0;
}

void _smpc_print_key_stdout(uint8_t* a, int length) {
  printf("%01X |  ", _smpc_test_bit(a, SMPC_KEY_POINTER));
  for (size_t i = 0; i < length; i++) {
    printf("%02X", a[i]);
  }
  printf("\n"); fflush(stdout);
}

void _smpc_print_wire_stdout(smpc_wire* w) {
  printf("v:  ");
  _smpc_print_key_stdout(w->v, SMPC_KEY_B_NUM);
  printf("k0: ");
  _smpc_print_key_stdout(w->k0, SMPC_KEY_B_NUM);
  printf("k1: ");
  _smpc_print_key_stdout(w->k1, SMPC_KEY_B_NUM);

}

void _smpc_set_input_wire_keys(smpc_graph* g) {
  uint8_t key[SMPC_KEY_B_NUM];
  smpc_wirelist_node* wireNode = g->input_wires->head;
  for (size_t i = 0; i < g->input_wires->length; i++) {
    yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, key);
    memcpy(wireNode->key->k0, key, SMPC_KEY_B_NUM * sizeof(uint8_t));
    yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, key);
    memcpy(wireNode->key->k1, key, SMPC_KEY_B_NUM * sizeof(uint8_t));
    if (_smpc_test_bit(wireNode->key->k0, SMPC_KEY_POINTER))
      ClearBit(wireNode->key->k1, SMPC_KEY_POINTER);
    else
      SetBit(wireNode->key->k1, SMPC_KEY_POINTER);
    wireNode = wireNode->next;
  }
}

void _smpc_a_not(smpc_evgatelist_node* p, int* tv) {
  switch (p->key->type) {
    case SMPC_EVGATE_TYPE_AND: {
      tv[0] = 0;
      tv[1] = 1;
      tv[2] = 0;
      tv[3] = 0;
      p->key->type = SMPC_EVGATE_TYPE_NOT_A;
      break;
    }
    case SMPC_EVGATE_TYPE_OR: {
      tv[0] = 1;
      tv[1] = 1;
      tv[2] = 0;
      tv[3] = 1;
      break;
    }
    case SMPC_EVGATE_TYPE_NAND: {
      tv[0] = 1;
      tv[1] = 0;
      tv[2] = 1;
      tv[3] = 1;
      p->key->type = SMPC_EVGATE_TYPE_NOT_A;
      break;
    }
    case SMPC_EVGATE_TYPE_NOR: {
      tv[0] = 0;
      tv[1] = 0;
      tv[2] = 1;
      tv[3] = 0;
      break;
    }
    case SMPC_EVGATE_TYPE_XOR: {
      tv[0] = 1;
      tv[1] = 0;
      tv[2] = 0;
      tv[3] = 1;
      p->key->type = SMPC_EVGATE_TYPE_XNOR;
      break;
    }
    case SMPC_EVGATE_TYPE_XNOR: {
      tv[0] = 0;
      tv[1] = 1;
      tv[2] = 1;
      tv[3] = 0;
      p->key->type = SMPC_EVGATE_TYPE_XOR;
      break;
    }
    default: {
      break;
    }
  }
}

void _smpc_b_not(smpc_evgatelist_node* p, int* tv) {
  switch (p->key->type) {
    case SMPC_EVGATE_TYPE_AND: {
      tv[0] = 0;
      tv[1] = 0;
      tv[2] = 1;
      tv[3] = 0;
      p->key->type = SMPC_EVGATE_TYPE_NOT_A;
      break;
    }
    case SMPC_EVGATE_TYPE_OR: {
      tv[0] = 1;
      tv[1] = 0;
      tv[2] = 1;
      tv[3] = 1;
      break;
    }
    case SMPC_EVGATE_TYPE_NAND: {
      tv[0] = 1;
      tv[1] = 1;
      tv[2] = 0;
      tv[3] = 1;
      p->key->type = SMPC_EVGATE_TYPE_NOT_A;
      break;
    }
    case SMPC_EVGATE_TYPE_NOR: {
      tv[0] = 0;
      tv[1] = 1;
      tv[2] = 0;
      tv[3] = 0;
      break;
    }
    case SMPC_EVGATE_TYPE_XOR: {
      tv[0] = 1;
      tv[1] = 0;
      tv[2] = 0;
      tv[3] = 1;
      p->key->type = SMPC_EVGATE_TYPE_XNOR;
      break;
    }
    case SMPC_EVGATE_TYPE_XNOR: {
      tv[0] = 0;
      tv[1] = 1;
      tv[2] = 1;
      tv[3] = 0;
      p->key->type = SMPC_EVGATE_TYPE_XOR;
      break;
    }
    default: {
      break;
    }
  }
}

void _smpc_none_not(smpc_evgatelist_node* p, int* tv) {
  switch (p->key->type) {
    case SMPC_EVGATE_TYPE_AND: {
      tv[0] = 0;
      tv[1] = 0;
      tv[2] = 0;
      tv[3] = 1;
      break;
    }
    case SMPC_EVGATE_TYPE_OR: {
      tv[0] = 0;
      tv[1] = 1;
      tv[2] = 1;
      tv[3] = 1;
      break;
    }
    case SMPC_EVGATE_TYPE_NAND: {
      tv[0] = 1;
      tv[1] = 1;
      tv[2] = 1;
      tv[3] = 0;
      break;
    }
    case SMPC_EVGATE_TYPE_NOR: {
      tv[0] = 1;
      tv[1] = 0;
      tv[2] = 0;
      tv[3] = 0;
      break;
    }
    case SMPC_EVGATE_TYPE_XOR: {
      tv[0] = 0;
      tv[1] = 1;
      tv[2] = 1;
      tv[3] = 0;
      break;
    }
    case SMPC_EVGATE_TYPE_XNOR: {
      tv[0] = 1;
      tv[1] = 0;
      tv[2] = 0;
      tv[3] = 1;
      break;
    }
    default: {
      break;
    }
  }
}

void _smpc_e_not(smpc_evgatelist_node* p, int* tv) {
  p->key->not = SMPC_NOT_NONE;
  switch (p->key->type) {
    case SMPC_EVGATE_TYPE_AND: {
      p->key->type = SMPC_EVGATE_TYPE_NOR;
      break;
    }
    case SMPC_EVGATE_TYPE_OR: {
      p->key->type = SMPC_EVGATE_TYPE_NAND;
      break;
    }
    case SMPC_EVGATE_TYPE_NAND: {
      p->key->type = SMPC_EVGATE_TYPE_OR;
      break;
    }
    case SMPC_EVGATE_TYPE_NOR: {
      p->key->type = SMPC_EVGATE_TYPE_AND;
      break;
    }
    default: {
      break;
    }
  }
}

void _smpc_evolve_not(smpc_evgatelist_node* p, int* tv) {
  switch (p->key->not) {
    case SMPC_NOT_A: {
      _smpc_a_not(p, tv);
      break;
    }
    case SMPC_NOT_B: {
      _smpc_b_not(p, tv);
      break;
    }
    case SMPC_NOT_E: {
      _smpc_e_not(p, tv);
    }
    default: {
      _smpc_none_not(p, tv);
      break;
    }
  }
}

void smpc_set_gate_keys(smpc_graph* g, uint8_t grrFlag) {
  if (g->sorted_gates->length == 0)
    smpc_topological_sort(g);
  smpc_evgatelist_node* p = g->sorted_gates->tail;
    if (smpc_initialize() != SMPC_SUCCESS) perror("Error on initialize smpc");
  _smpc_set_input_wire_keys(g);
  while (p != NULL) {
    int tv[4];
    _smpc_evolve_not(p, tv);
    if (!grrFlag)
      _smpc_set_gate_lut_keys(p, tv);
    else
      _smpc_set_grr3_lut_row(p, tv);
    if (_smpc_is_output(g, p) == 1){
      if (_smpc_test_bit(p->key->out->k0 , SMPC_KEY_POINTER) == 0) {
        p->key->out->state = true;
      }
      else p->key->out->state = false;
    }
    p = p->prev;
  }
}

void _smpc_set_grr3_zero_row_fx(smpc_evgatelist_node* p, int* tv, uint8_t* delta) {
  uint8_t tk[(SMPC_KEY_B_NUM*2) + 2];
  uint8_t index = 0;
  if(_smpc_test_bit(p->key->in1->k1, SMPC_KEY_POINTER) == 0)
    index = 1;
  if (_smpc_test_bit(p->key->in0->k1, SMPC_KEY_POINTER) == 0)
    index += 2;
  uint8_t *a, *b;
  switch (index) {
    case 0:{
      _smpc_copy_string_bh(tk, p->key->in0->k0, p->key->in1->k0, p->key->id);
      a = p->key->in0->k0;
      b = p->key->in1->k0;
      break;
    }
    case 1:{
      _smpc_copy_string_bh(tk, p->key->in0->k0, p->key->in1->k1, p->key->id);
      a = p->key->in0->k0;
      b = p->key->in1->k1;
      break;
    }
    case 2:{
      _smpc_copy_string_bh(tk, p->key->in0->k1, p->key->in1->k0, p->key->id);
      a = p->key->in0->k1;
      b = p->key->in1->k0;
      break;
    }
    case 3:{
      _smpc_copy_string_bh(tk, p->key->in0->k1, p->key->in1->k1, p->key->id);
      a = p->key->in0->k1;
      b = p->key->in1->k1;
      break;
    }
  }
  uint8_t* theChosenOne;
  uint8_t* theOtherOne;
  if (tv[index] == 0) {
    theChosenOne = p->key->out->k0;
    theOtherOne = p->key->out->k1;
  }
  else {
    theChosenOne = p->key->out->k1;
    theOtherOne = p->key->out->k0;
  }
  ClearAll(p->key->lut[0], SMPC_KEY_B_NUM);
  struct sha3_256_ctx context;
  sha3_256_init (&context);
  sha3_256_update (&context, (SMPC_KEY_B_NUM*2) + 2, tk);
  sha3_256_digest(&context, SMPC_KEY_B_NUM, theChosenOne);
  _smpc_set_lut_pp(p, _smpc_test_bit(a, SMPC_KEY_POINTER),
                   _smpc_test_bit(b, SMPC_KEY_POINTER),
                   0
                  );
  if (_smpc_test_bit(theChosenOne, SMPC_KEY_POINTER) == 0) {
    SetBit(theOtherOne, SMPC_KEY_POINTER);
  }
  else {
    ClearBit(theOtherOne, SMPC_KEY_POINTER);
  }
  _smpc_xor(theOtherOne, theChosenOne, delta);
}

void _smpc_set_grr3_lut_row_fx(smpc_evgatelist_node* p, int* tv, uint8_t* delta) {
  _smpc_set_grr3_zero_row_fx(p, tv, delta);
  _smpc_set_grr3_other_row(p, tv);
//  _smpc_permute_grr3_lut_row(p);
}

void _smpc_set_gate_lut_keys_fx(smpc_evgatelist_node* p, int* tv, uint8_t* delta) {
  uint8_t key[SMPC_KEY_B_NUM];
  yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, key);
  memcpy(p->key->out->k0, key, SMPC_KEY_B_NUM * sizeof(uint8_t));
  _smpc_xor(p->key->out->k1, p->key->out->k0, delta);
  if(_smpc_test_bit(p->key->out->k0, SMPC_KEY_POINTER) == 0)
    SetBit(p->key->out->k1, SMPC_KEY_POINTER);
  else
    ClearBit(p->key->out->k1, SMPC_KEY_POINTER);
    uint8_t x, y, z;
    uint8_t *a, *b;
    for (size_t i = 0; i < 4; i++) {
      switch (i) {
        case 0:{
          a = p->key->in0->k0;
          b = p->key->in1->k0;
          x = 0;
          y = 0;
          break;
        }
        case 1:{
          a = p->key->in0->k0;
          b = p->key->in1->k1;
          x = 0;
          y = 1;
          break;
        }
        case 2:{
          a = p->key->in0->k1;
          b = p->key->in1->k0;
          x = 1;
          y = 0;
          break;
        }
        case 3:{
          a = p->key->in0->k1;
          b = p->key->in1->k1;
          x = 1;
          y = 1;
          break;
        }
      }
      z = tv[i];
      _smpc_set_lut_row(p, x, y, z, i);
    }
    _smpc_permute_lut_row(p);
}

void _smpc_set_HG_and_gate(smpc_evgatelist_node* p, uint8_t* delta) {
  uint8_t pb = _smpc_test_bit(p->key->in1->k0, SMPC_KEY_POINTER);
  uint8_t pa = _smpc_test_bit(p->key->in0->k0, SMPC_KEY_POINTER);
  if(!pb) ClearAll(&pb, 1);
  else pb = 0xFF;
  if(!pa) ClearAll(&pa, 1);
  else pa = 0xFF;
  uint8_t hwa0[SMPC_KEY_B_NUM];
  uint8_t hwa1[SMPC_KEY_B_NUM];
  uint8_t hwb0[SMPC_KEY_B_NUM];
  uint8_t hwb1[SMPC_KEY_B_NUM];
  struct sha3_256_ctx context;
  sha3_256_init (&context);
  sha3_256_update (&context, SMPC_KEY_B_NUM, p->key->in0->k0);
  sha3_256_digest(&context, SMPC_KEY_B_NUM, hwa0);
  sha3_256_init (&context);
  sha3_256_update (&context, SMPC_KEY_B_NUM, p->key->in0->k1);
  sha3_256_digest(&context, SMPC_KEY_B_NUM, hwa1);
  sha3_256_init (&context);
  sha3_256_update (&context, SMPC_KEY_B_NUM, p->key->in1->k0);
  sha3_256_digest(&context, SMPC_KEY_B_NUM, hwb0);
  sha3_256_init (&context);
  sha3_256_update (&context, SMPC_KEY_B_NUM, p->key->in1->k1);
  sha3_256_digest(&context, SMPC_KEY_B_NUM, hwb1);
  _smpc_xor(p->key->lut[1], hwa0, hwa1);
  uint8_t tmp[SMPC_KEY_B_NUM];
  for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
    tmp[i] = pb & delta[i];
  }
  _smpc_xor(p->key->lut[1], p->key->lut[1], tmp);
  uint8_t wg0[SMPC_KEY_B_NUM];
  for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
    tmp[i] = pa & p->key->lut[1][i];
  }
  _smpc_xor(wg0, hwa0, tmp);
  _smpc_xor(p->key->lut[3], hwb0, hwb1);
  _smpc_xor(p->key->lut[3], p->key->lut[3], p->key->in0->k0);
  uint8_t we0[SMPC_KEY_B_NUM];
  _smpc_xor(tmp, p->key->lut[3], p->key->in0->k0);
  for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
    tmp[i] = pb & tmp[i];
  }
  _smpc_xor(we0, hwb0, tmp);
  uint8_t* oa = p->key->out->k0;
  uint8_t* ob = p->key->out->k1;
  if (p->key->type == SMPC_EVGATE_TYPE_NAND) {
    oa = p->key->out->k1;
    ob = p->key->out->k0;
  }
  _smpc_xor(oa, wg0, we0);
  _smpc_xor(ob, oa, delta);
}


void smpc_set_gate_keys_freeXOR(smpc_graph* g, uint8_t flag) {
  if (g->sorted_gates->length == 0)
    smpc_topological_sort(g);
  smpc_evgatelist_node* p = g->sorted_gates->tail;
  smpc_wirelist_node* wireNode = g->input_wires->head;
  uint8_t key[SMPC_KEY_B_NUM];
  uint8_t delta[SMPC_KEY_B_NUM];
  uint8_t point[SMPC_KEY_B_NUM];
  if (smpc_initialize() != SMPC_SUCCESS) perror("Error on initialize smpc");
  yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, delta);
  SetBit(delta, SMPC_KEY_POINTER);
  for (size_t i = 0; i < g->input_wires->length; i++) {
    yarrow256_random(&SMPC_PRNG, SMPC_KEY_B_NUM, key);
    for (size_t j = 0; j < SMPC_KEY_B_NUM; j++) {
      wireNode->key->k0[j] = key[j];
    }
    if (point[0] % 2 == 0) {
      ClearBit(wireNode->key->k0, SMPC_KEY_POINTER);
      SetBit(wireNode->key->k1, SMPC_KEY_POINTER);
    }
    else {
      ClearBit(wireNode->key->k1, SMPC_KEY_POINTER);
      SetBit(wireNode->key->k0, SMPC_KEY_POINTER);
    }
    _smpc_xor(wireNode->key->k1, wireNode->key->k0, delta);
    wireNode = wireNode->next;
  }
  while (p != NULL) {
    int tv[4];
    switch (p->key->type) {
      case SMPC_EVGATE_TYPE_XOR: {
        _smpc_evolve_not(p, tv);
        _smpc_set_free_xor_gate(p, delta);
        break;
      }
      case SMPC_EVGATE_TYPE_XNOR: {
        _smpc_evolve_not(p, tv);
        _smpc_set_free_xor_gate(p, delta);
        break;
      }
      default: {
        _smpc_evolve_not(p, tv);
        break;
      }
    }
    if(p->key->type != SMPC_EVGATE_TYPE_XOR &&
       p->key->type != SMPC_EVGATE_TYPE_XNOR)
      if (!_smpc_test_bit(&flag, 0))
        _smpc_set_gate_lut_keys_fx(p, tv, delta);
      else {
        if (!_smpc_test_bit(&flag, 1) ||
            (p->key->type != SMPC_EVGATE_TYPE_AND &&
             p->key->type != SMPC_EVGATE_TYPE_NAND))
          _smpc_set_grr3_lut_row_fx(p, tv, delta);
        else
          _smpc_set_HG_and_gate(p, delta);
      }
    if (_smpc_is_output(g, p) == 1){
      if (_smpc_test_bit(p->key->out->k0 , SMPC_KEY_POINTER) == 0) {
        p->key->out->state = true;
      }
      else p->key->out->state = false;
    }
    p = p->prev;
  }
}
void _smpc_print_gate_stdout(smpc_evgate* g) {
  switch (g->type) {
    case SMPC_EVGATE_TYPE_AND: {
      printf("AND\n");
      break;
    }
    case SMPC_EVGATE_TYPE_OR: {
      printf("OR\n");
      break;
    }
    case SMPC_EVGATE_TYPE_NAND: {
      printf("NAND\n");
      break;
    }
    case SMPC_EVGATE_TYPE_NOR: {
      printf("NOR\n");
      break;
    }
    case SMPC_EVGATE_TYPE_XOR: {
      printf("XOR\n");
      break;
    }
    case SMPC_EVGATE_TYPE_XNOR: {
      printf("XNOR\n");
      break;
    }
    default: {
      break;
    }
  }
  printf("lut: \n");
  for (size_t i = 0; i < 4; i++) {
    printf("%01X | %01X || ", _smpc_test_bit(&g->lut[i][SMPC_KEY_B_NUM], 0), _smpc_test_bit(&g->lut[i][SMPC_KEY_B_NUM], 1));
    for (size_t j = 0; j < SMPC_KEY_B_NUM; j++) {
      printf("%02X", g->lut[i][j]);
    }
    printf("\n"); fflush(stdout);
  }
  printf("\n");
  printf("in0:\n");
  _smpc_print_wire_stdout(g->in0);
  printf("in1:\n");
  _smpc_print_wire_stdout(g->in1);
  printf("out:\n");
  _smpc_print_wire_stdout(g->out);
  printf("\n");
}

void smpc_print_gc_stdout(smpc_graph* g, uint8_t fxFlag) {
  if (g->sorted_gates->length == 0)
    smpc_topological_sort(g);
  smpc_evgatelist_node* p = g->sorted_gates->tail;
  int i = 0;
  while (p != NULL) {
    printf("Gate %d:\n", p->key->id);
    if (!fxFlag) {
      _smpc_print_gate_stdout(p->key);
    }
    else {
      if (p->key->type == SMPC_EVGATE_TYPE_XOR) {
        printf("XOR\n");
        printf("in0:\n");
        _smpc_print_wire_stdout(p->key->in0);
        printf("in1:\n");
        _smpc_print_wire_stdout(p->key->in1);
        printf("out:\n");
        _smpc_print_wire_stdout(p->key->out);
        printf("\n");
      }
      else {
        _smpc_print_gate_stdout(p->key);
      }
    }
    i++;
    p = p->prev;
    printf("\n");
  }
}

int smpc_evaluate_offline_gc_no(smpc_graph* g, uint8_t* output) {
  struct sha3_256_ctx context;
  uint8_t zerouint8_t;
  ClearAllU(&zerouint8_t);
  if (g->sorted_gates->length == 0)
    return SMPC_EVALUATION_ERROR_GC_NOT_SORTED;
  smpc_evgatelist_node* p = g->sorted_gates->tail;
  while (p != NULL) {
    int c = 0;
    uint8_t tmp[(SMPC_KEY_B_NUM*2) + 2];
    uint8_t tt[SMPC_KEY_B_NUM];
    for (size_t i = 0; i < 4; i++) {
      if (_smpc_lut_pp_metch(p, p->key->in0->v, p->key->in1->v, i) == 1) {
        c = i;
        i = 4;
      }
    }
    _smpc_copy_string_bh(tmp, p->key->in0->v, p->key->in1->v, p->key->id);
    sha3_256_init (&context);
    sha3_256_update (&context, (SMPC_KEY_B_NUM*2) + 2, tmp);
    sha3_256_digest(&context, SMPC_KEY_B_NUM, tt);
    for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
      p->key->out->v[i] = tt[i] ^ p->key->lut[c][i];
    }
    p = p->prev;
  }
  smpc_wirelist_node* wireNode = g->output_wires->head;
  uint8_t bit = 0;
  for (size_t i = 0; i < g->output_wires->length; i++) {
    if (wireNode->key->state)
      bit = _smpc_test_bit(wireNode->key->v, SMPC_KEY_POINTER);
    else
      bit = 1 - _smpc_test_bit(wireNode->key->v, SMPC_KEY_POINTER);
    output[i/8] |= (bit << (i % 8));
    wireNode = wireNode->next;
  }
  return 0;
}

int smpc_evaluate_offline_gc_fx(smpc_graph* g, uint8_t* output, uint8_t HGflag) {
  struct sha3_256_ctx context;
  uint8_t zerouint8_t;
  ClearAllU(&zerouint8_t);
  if (g->sorted_gates->length == 0)
    return SMPC_EVALUATION_ERROR_GC_NOT_SORTED;
  smpc_evgatelist_node* p = g->sorted_gates->tail;
  while (p != NULL) {
    int c = 0;
    uint8_t tmp[(SMPC_KEY_B_NUM*2) + 2];
    uint8_t tt[SMPC_KEY_B_NUM];
    switch (p->key->type) {
      case SMPC_EVGATE_TYPE_XOR: {
        for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
          p->key->out->v[i] = p->key->in0->v[i] ^ p->key->in1->v[i];
        }
        break;
      }
      case SMPC_EVGATE_TYPE_XNOR: {
        for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
          p->key->out->v[i] = p->key->in0->v[i] ^ p->key->in1->v[i];
        }
        break;
      }
      case SMPC_EVGATE_TYPE_AND: {
        if(HGflag != 0) {
          uint8_t sb = _smpc_test_bit(p->key->in1->v, SMPC_KEY_POINTER);
          uint8_t sa = _smpc_test_bit(p->key->in0->v, SMPC_KEY_POINTER);
          if(!sb) ClearAll(&sb, 1);
          else sb = 0xFF;
          if(!sa) ClearAll(&sa, 1);
          else sa = 0xFF;
          uint8_t hwa[SMPC_KEY_B_NUM];
          uint8_t hwb[SMPC_KEY_B_NUM];
          uint8_t wg[SMPC_KEY_B_NUM];
          uint8_t we[SMPC_KEY_B_NUM];
          uint8_t tmp[SMPC_KEY_B_NUM];
          sha3_256_init (&context);
          sha3_256_update (&context, SMPC_KEY_B_NUM, p->key->in0->v);
          sha3_256_digest(&context, SMPC_KEY_B_NUM, hwa);
          sha3_256_init (&context);
          sha3_256_update (&context, SMPC_KEY_B_NUM, p->key->in1->v);
          sha3_256_digest(&context, SMPC_KEY_B_NUM, hwb);
          for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
            tmp[i] = sa & p->key->lut[1][i];
          }
          _smpc_xor(wg, hwa, tmp);
          _smpc_xor(tmp, p->key->lut[3], p->key->in0->v);
          for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
            tmp[i] = sb & tmp[i];
          }
          _smpc_xor(we, hwb, tmp);
          _smpc_xor(p->key->out->v, wg, we);
          break;
        }
      }
      case SMPC_EVGATE_TYPE_NAND: {
        if(HGflag != 0) {
          uint8_t sb = _smpc_test_bit(p->key->in1->v, SMPC_KEY_POINTER);
          uint8_t sa = _smpc_test_bit(p->key->in0->v, SMPC_KEY_POINTER);
          if(!sb) ClearAll(&sb, 1);
          else sb = 0xFF;
          if(!sa) ClearAll(&sa, 1);
          else sa = 0xFF;
          uint8_t hwa[SMPC_KEY_B_NUM];
          uint8_t hwb[SMPC_KEY_B_NUM];
          uint8_t wg[SMPC_KEY_B_NUM];
          uint8_t we[SMPC_KEY_B_NUM];
          uint8_t tmp[SMPC_KEY_B_NUM];
          sha3_256_init (&context);
          sha3_256_update (&context, SMPC_KEY_B_NUM, p->key->in0->v);
          sha3_256_digest(&context, SMPC_KEY_B_NUM, hwa);
          sha3_256_init (&context);
          sha3_256_update (&context, SMPC_KEY_B_NUM, p->key->in1->v);
          sha3_256_digest(&context, SMPC_KEY_B_NUM, hwb);
          for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
            tmp[i] = sa & p->key->lut[1][i];
          }
          _smpc_xor(wg, hwa, tmp);
          _smpc_xor(tmp, p->key->lut[3], p->key->in0->v);
          for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
            tmp[i] = sb & tmp[i];
          }
          _smpc_xor(we, hwb, tmp);
          _smpc_xor(p->key->out->v, wg, we);
          break;
        }
      }
      default: {
        for (size_t i = 0; i < 4; i++) {
          if (_smpc_lut_pp_metch(p, p->key->in0->v, p->key->in1->v, i) == 1) {
            c = i;
            i = 4;
          }
        }
        _smpc_copy_string_bh(tmp, p->key->in0->v, p->key->in1->v, p->key->id);
        sha3_256_init (&context);
        sha3_256_update (&context, (SMPC_KEY_B_NUM*2) + 2, tmp);
        sha3_256_digest(&context, SMPC_KEY_B_NUM, tt);
        for (size_t i = 0; i < SMPC_KEY_B_NUM; i++) {
          p->key->out->v[i] = tt[i] ^ p->key->lut[c][i];
        }
        break;
      }
    }
    p = p->prev;
  }
  smpc_wirelist_node* wireNode = g->output_wires->head;
  uint8_t bit = 0;
  for (size_t i = 0; i < g->output_wires->length; i++) {
    if (wireNode->key->state)
      bit = _smpc_test_bit(wireNode->key->v, SMPC_KEY_POINTER);
    else
      bit = 1 - _smpc_test_bit(wireNode->key->v, SMPC_KEY_POINTER);
    output[i/8] |= (bit << (i % 8));
    wireNode = wireNode->next;
  }
  return 0;
}

int smpc_set_gc(smpc_graph* g) {
  switch (g->options) {
    case SMPC_GC_OPT_NO:{
      smpc_set_gate_keys(g, 0);
      break;
    }
    case SMPC_GC_OPT_GRR:{
      smpc_set_gate_keys(g, 1);
      break;
    }
    case SMPC_GC_OPT_FX:{
      smpc_set_gate_keys_freeXOR(g, 0);
      break;
    }
    case SMPC_GC_OPT_GRR_FX:{
      smpc_set_gate_keys_freeXOR(g, 1);
      break;
    }
    case SMPC_GC_OPT_GRR_FX_HG:{
      smpc_set_gate_keys_freeXOR(g, 3);
      break;
    }
    default:{
      return -1;
    }
  }
  return SMPC_SUCCESS;
}

int smpc_evaluate_offline_gc(smpc_graph* g, uint8_t* output) {
  switch (g->options) {
    case SMPC_GC_OPT_NO:{
      smpc_evaluate_offline_gc_no(g, output);
      break;
    }
    case SMPC_GC_OPT_GRR:{
      smpc_evaluate_offline_gc_no(g, output);
      break;
    }
    case SMPC_GC_OPT_FX:{
      smpc_evaluate_offline_gc_fx(g, output, 0);
      break;
    }
    case SMPC_GC_OPT_GRR_FX:{
      smpc_evaluate_offline_gc_fx(g, output, 0);
      break;
    }
    case SMPC_GC_OPT_GRR_FX_HG:{
      smpc_evaluate_offline_gc_fx(g, output, 1);
      break;
    }
    default:{
      return -1;
    }
  }
  return SMPC_SUCCESS;
}
